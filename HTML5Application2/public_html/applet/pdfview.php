<?php
    
    session_start();    

    if (!(isset($_SESSION['username'])))
    {
      die('<h1>Direct File Access Prohibited</h1>');
    }
    
    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';


    $conn = mysqli_connect($servername ,$username , $password , $database);

if(isset($_COOKIE['customername']))
{
    $customername = $_COOKIE['customername'];
     if($customername == 'null')
    {
        $customername = '';
    }
}
if(isset($_COOKIE['invoicenumber'])) 
{
        $invoicenumber = $_COOKIE['invoicenumber'];
        if($invoicenumber == 'null')
        {
          $invoicenumber = '';
        }
}
if(isset($_COOKIE['purchasenumber']))
{
    $purchasenumber = $_COOKIE['purchasenumber'];
    if($purchasenumber == 'null')
    {
         $purchasenumber = '';
    }
}
if(isset($_COOKIE['orderdate']))
{
    $orderdate = $_COOKIE['orderdate'];
    if($orderdate == 'null')
    {
         $orderdate = '';
    }
}
if(isset($_COOKIE['amount']))
{
    $amount = $_COOKIE['amount'];
    if($amount == 'null')
    {
         $amount = '';
    }
}
if(isset($_COOKIE['customernumber']))
{
    $customernumber = $_COOKIE['customernumber'];
    if($customernumber == 'null')
    {
         $customernumber = '';
    }
}
if(isset($_COOKIE['podate']))
{
    $podate = $_COOKIE['podate'];
    if($podate == 'null')
    {
         $podate = '';
    }
}
if(isset($_COOKIE['shipmentdate']))
{
    $shipmentdate = $_COOKIE['shipmentdate'];
    if($shipmentdate == 'null')
    {
         $shipmentdate = '';
    }
}
if(isset($_COOKIE['ordernumber']))
{
    $ordernumber = $_COOKIE['ordernumber'];
    if($ordernumber == 'null')
    {
         $ordernumber = '';
    }
}
if(isset($_COOKIE['ponumber']))
{
    $ponumber = $_COOKIE['ponumber'];
    if($ponumber == 'null')
    {
         $ponumber = '';
    }
}

    if(isset($_COOKIE['company']))
    {
        if($_COOKIE['document'] == 'Invoice')
        {
             $sql = "SELECT file,filename,comments FROM productdocuments WHERE id = '".$_COOKIE['id']."' AND company = '".$_COOKIE['company']."' AND documentclass = '".$_COOKIE['document']."' AND customername = '".$customername."' AND invoicenumber = '".$invoicenumber."' AND purchasenumber = '".$purchasenumber."' AND orderdate = '".$orderdate."' AND amount = '".$amount."'";
        }
        else if($_COOKIE['document'] == 'Shipment')
        {
           $sql = "SELECT file,filename,comments FROM productdocuments WHERE id = '".$_COOKIE['id']."' AND company = '".$_COOKIE['company']."' AND documentclass = '".$_COOKIE['document']."' AND shipmentdate = '".$shipmentdate."' AND invoicenumber = '".$invoicenumber."' AND ordernumber = '".$ordernumber."' AND orderdate = '".$orderdate."'";
        }
        else
        {
           $sql = "SELECT file,filename,comments FROM productdocuments WHERE id = '".$_COOKIE['id']."' AND company = '".$_COOKIE['company']."' AND documentclass = '".$_COOKIE['document']."' AND customername = '".$customername."' AND customernumber = '".$customernumber."' AND ponumber = '".$ponumber."' AND podate = '".$podate."'";
        }
    
        $result = mysqli_query($conn,$sql);

        $check = mysqli_num_rows($result);
        if($check > 0)
        {
        	$row = mysqli_fetch_assoc($result);
        	$file = $row['file'];
            $filename = $row['filename'];
            $comments = $row['comments'];
        }
    }
    else
    {
        header("Location: search");
    }
?>  


<!DOCTYPE html>
<html>
	<head>
		<title>DOCUMENT VIEW</title>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="stylesheet" type="text/css" href="css/pdfviewcss.css">
         <script src="scripts/script.js"></script>
         <link rel="shortcut icon" href="images/appletlogo.png">
	</head>
	<body>

          <div class="dropdown">
            <button class="dropbtn" style = 'width : 150px;'>Options</button>
            <div class="dropdown-content">
                <a onclick = 'pdfupdate()'>Update</a>
                <a onclick = 'downloadpdf()'>Download</a>
                <a onclick = 'pdfdelete()'>Delete</a>
            </div>
        </div>
        <p style = 'position: absolute; top : 0px;left : 280px;'>COMMENTS : <?php if(isset($comments)) echo $comments; ?></p>
        <div style = 'display: flex;'>
            <div>
    		  <iframe id ='frame' src = "data:application/pdf;base64,<?php echo base64_encode($file); ?>" ></iframe><br>

    		  <a hidden id = "pdfdown" href ="data:application/pdf;base64,<?php echo base64_encode($file); ?>" download = "<?php echo $filename; ?>.pdf">download</a>

            </div>
            <div id = 'editindex'>
                <div style = "display: flex;background : #F5F5F5;">
                        <i style = 'margin : 17px;font-size: 20px;color : lightblue;' class="fa fa-edit" aria-hidden="true"></i>
                        <h4 style = 'color:silver;'>Edit indexes</h4>
                </div>
                <form action = 'update/documentupdate.php' method = 'POST' enctype="multipart/form-data">
                    <?php 

                        if($_COOKIE['document'] == 'Invoice')
                        {
                         echo '<br>
                         <select name="company">
                            <option value="none">Company</option>
                            <option value="VeeFish Nets">VeeFish Nets</option>
                            <option value="Seba Nets">Seba Nets</option>
                            <option value="Vee Nets">Vee Nets</option>
                        </select><br>  
                        
                        <input type="file" name="file" accept="application/pdf"  placeholder="choose file"><br> 
                        <input type="text" name="filename" placeholder="filename" autocomplete="off"><br>
                        <input type="text" name="key" placeholder="key value" autocomplete="off"><br> 
                        <input type="text" name="val1" placeholder="customer name" autocomplete="off"><br> 
                        <input type="text" name="val2" placeholder="Invoice number" autocomplete="off"><br>
                        <input type="text"  onfocus="datechange(this)" onblur="textchange(this)" name="val3"  autocomplete="off" placeholder = "Order date"><br>
                        <input type="text" name="val4" placeholder="Purchase number" autocomplete="off"><br>
                        <input type="text" name="val5" placeholder="Comments" autocomplete="off"><br>
                        <input type="text" name="val6" placeholder="Amount" autocomplete="off"><br>

                        <button type = "submit">Update</button>
                        <button type = "reset" style = "margin-top : 4px;" onclick = "pdfupdateclose()">Cancel</button><br>';
                        }
                        else if($_COOKIE['document'] == 'Purchase Orders')
                        {
                            echo '<br>
                        <select name="company">
                            <option value="none">Company</option>
                            <option value="VeeFish Nets">VeeFish Nets</option>
                            <option value="Seba Nets">Seba Nets</option>
                            <option value="Vee Nets">Vee Nets</option>
                        </select><br>  
                        
                        <input type="file" accept="application/pdf" name="file" placeholder="choose file"><br> 
                        <input type="text" name="filename" placeholder="filename" autocomplete="off"><br>
                        <input type="text" name="key" placeholder="key value" autocomplete="off"><br> 
                        <input type="text" name="val1" placeholder="customer name" autocomplete="off"><br> 
                        <input type="text" name="val2" placeholder="customer number" autocomplete="off"><br>
                        <input type="text"  onfocus="datechange(this)" onblur="textchange(this)" name="val3"  autocomplete="off" placeholder = "PO date"><br>
                        <input type="text" name="val4" placeholder="PO number" autocomplete="off"><br>
                        <input type="text" name="val5" placeholder="Comments" autocomplete="off"><br>
                        <input type="text" name="val6" placeholder="Amount" autocomplete="off"><br>
                        
                        <button type = "submit">Update</button>
                        <button type = "reset" style = "margin-top : 4px;" onclick = "pdfupdateclose()">Cancel</button><br>';
                        }
                        else
                        {
                            echo '<br>
                        <select name="company">
                            <option value="none">Company</option>
                            <option value="Seba Nets">Seba Nets</option>
                            <option value="VeeFish Nets">VeeFish Nets</option>
                            <option value="Seba Nets">Seba Nets</option>
                            <option value="Vee Nets">Vee Nets</option>
                        </select><br>  
                        
                        <input type="file" accept="application/pdf" name="file" placeholder="choose file"><br> 
                        <input type="text" name="filename" placeholder="filename" autocomplete="off"><br>
                        <input type="text" name="key" placeholder="key value" autocomplete="off"><br> 
                        <input type="text"  onfocus="datechange(this)" onblur="textchange(this)" name="val1"  autocomplete="off" placeholder = "Shipment date"><br> 
                        <input type="text" name="val2" id="inumbox" placeholder="Invoice number" autocomplete="off"><br>
                        <input type="text"  onfocus="datechange(this)" onblur="textchange(this)" name="val3"  autocomplete="off" placeholder = "Order date"><br>
                        <input type="text" name="val4" placeholder="Order number" autocomplete="off"><br>
                        <input type="text" name="val5" placeholder="Comments" autocomplete="off"><br>

                        <button type = "submit">Update</button>
                        <button type = "reset" style = "margin-top : 4px;" onclick = "pdfupdateclose()">Cancel</button><br>';
                        }

                    ?>
                </form>
            </div>
        </div>
	</body>
</html>

<script>
   function datechange(val)
   {
     val.type = 'date';
     val.open();
   }
   function textchange(val)
   {
     val.type = 'text';
   }


   function pdfdelete()
   {   
     var val = prompt("Need admin password");
        if(val != '' && val == "<?php if(isset($_SESSION['adminpassword'])){echo $_SESSION['adminpassword'];} ?>")
        {      
            window.location.replace('delete/documentdelete.php');
        }
        else{
            if(val != null)
            {
                alert('Worng password');
                pdfdelete();
            }
        }
   }
</script>
