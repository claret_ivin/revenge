<?php
  
    session_start();    

    if (!(isset($_SESSION['username'])))
    {
      die('<h1>Direct File Access Prohibited</h1>');
    }
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Main</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/maincss.css">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="shortcut icon" href="applet.jpg">
        <link rel="shortcut icon" href="images/appletlogo.png">
    </head>
    <body>
        
        <div class = 'navbar'>
            <img src = "images/appletlogo.png"/>
            <h3 style = 'color : white;font-family: Georgia;margin : 20px;'>APPLET<br><span style = 'margin : 10px;font-size:10px'>-εκδίκηση-</span></h3>
            <a href="#" style = 'margin-left : 700px;'>ContactUs</a>
            <a href="logout.php" style = 'margin-left : 50px;'>Logout</a>
        </div>

        <div class = 'welcome'>
            <i class="fas fa-user"></i><br>
            Welcome <?php echo $_SESSION['username'];?>!
        </div>

        <div class="container">
    
             <div class="box">
                <img src = "images/documents-main.jpg" >
                <form>
                    <input type="submit" name="" value="Product Documents" formaction="dochome">
                </form>
            </div>
            <div class="box">
                <img src = "images/staff-main.jpg" > 
                <form>
                    <input type="submit" name="" value="Employee Documents" formaction="staff-home">  
                </form>
            </div>           
            
        </div>
          
    </body>
</html>
<style>
    .navbar img{
    width : 60px;
    height : 60px;
    padding : 0px;
    border-radius : 50%;
    margin-left : 260px;
    border : none;
}
</style>
