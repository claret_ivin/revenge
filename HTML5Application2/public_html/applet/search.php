 
<?php
  
  session_start();    

    if (!(isset($_SESSION['username'])))
    {
      die('<h1>Direct File Access Prohibited</h1>');
    }
    
    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';

    $conn = mysqli_connect($servername ,$username , $password , $database);

    if(isset($_GET['company']) || isset($_GET['document']))
    {
        $company = $_GET['company'];
        $document = $_GET['document'];

        if($document == 'Invoice')
        {
             $sql = "SELECT id,company,documentclass,customername,invoicenumber,purchasenumber,orderdate,amount,filename,keyvalues FROM productdocuments WHERE company = '".$company."' AND documentclass = '".$document."'";
        }
        else if($document == 'Shipment')
        {
            $sql = "SELECT id,company,documentclass,shipmentdate,ordernumber,orderdate,invoicenumber,filename,keyvalues FROM productdocuments WHERE company = '".$company."' AND documentclass = '".$document."'";
        }
        else
        {
            $sql = "SELECT id,company,documentclass,customername,customernumber,ponumber,podate,amount,filename,keyvalues FROM productdocuments WHERE company = '".$company."' AND documentclass = '".$document."'";
        }

        if(!empty($_GET['key']))
        {
             $sql .= " AND keyvalues = '".$_GET['key']."'";
        }
        if(!empty($_GET['cname']))
        {
             $sql .= " AND customername = '".$_GET['cname']."'";
        }
        if(!empty($_GET['cnumber']))
        {
             $sql .= " AND customernumber = '".$_GET['cnumber']."'";
        }
        if(!empty($_GET['ponum']))
        {
             $sql .= " AND ponumber = '".$_GET['ponum']."'";
        }
        if(!empty($_GET['invnum']))
        {
             $sql .= " AND invoicenumber = '".$_GET['invnum']."'";
        }
        if(!empty($_GET['ordernum']))
        {
             $sql .= " AND ordernumber = '".$_GET['ordernum']."'";
        }
        if(!empty($_GET['orderdate']))
        {
             $sql .= " AND orderdate = '".$_GET['orderdate']."'";
        }
        if(!empty($_GET['podate']))
        {
             $sql .= " AND podate = '".$_GET['podate']."'";
        }
        if(!empty($_POST['shipdate']))
        {
             $sql .= " AND shipmentdate = '".$_GET['shipmentdate']."'";
        }
        if(!empty($_GET['fromdate']) && !empty($_GET['todate']))
        {
             $sql .= " AND dates BETWEEN '".$_GET['fromdate']."'";
        }
        if(!empty($_GET['fromdate']) && !empty($_GET['todate']))
        {
             $sql .= " AND '".$_GET['todate']."'";
        }
       
        $result = mysqli_query($conn,$sql);

        $check = mysqli_num_rows($result);
        if($check <= 0)
        {
            $error = true;
        }
    }
    else
    {
      $check = 0;
      $error = false;
    }

?>

<!DOCTYPE html>

<html>
    <head>
        <title>Search</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/dochomecs.css">
        <link rel="stylesheet" type="text/css" href="css/searchcss.css">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="scripts/script2.js"></script>
        <link rel="shortcut icon" href="images/appletlogo.png">

        <script>
        
            function tablevalues(val)
            {
                document.cookie = "id = "+ val.childNodes[0].innerHTML;
                document.cookie = "company = "+ val.childNodes[1].innerHTML;
                document.cookie = "document = "+ val.childNodes[2].innerHTML;
                if(val.childNodes[2].innerHTML == 'Invoice')
                {
                    document.cookie = "customername = "+ val.childNodes[3].innerHTML;
                    document.cookie = "invoicenumber = "+ val.childNodes[4].innerHTML;
                    document.cookie = "purchasenumber = "+ val.childNodes[5].innerHTML;
                    document.cookie = "orderdate = "+ val.childNodes[6].innerHTML;
                    document.cookie = "amount = "+ val.childNodes[7].innerHTML;
                }
                else if(val.childNodes[2].innerHTML == 'Purchase Orders')
                {
                    document.cookie = "customername = "+ val.childNodes[3].innerHTML;
                    document.cookie = "customernumber = "+ val.childNodes[4].innerHTML;
                    document.cookie = "ponumber = "+ val.childNodes[5].innerHTML;
                    document.cookie = "podate = "+ val.childNodes[6].innerHTML;
                }
                else
                {
                    document.cookie = "shipmentdate = "+ val.childNodes[3].innerHTML;
                    document.cookie = "ordernumber = "+ val.childNodes[4].innerHTML;
                    document.cookie = "invoicenumber = "+ val.childNodes[5].innerHTML;
                    document.cookie = "orderdate = "+ val.childNodes[6].innerHTML;
                }
                window.location.href = 'pdfview';
            }
        </script>
    </head>
    <body>
       <div class="wrapper">   
        <div class="sidebar">
            <header>Product Details</header>
            <ul>
                <li><a href="dochome"><i class="fas fa-home"></i>Home</a></li>
                <li><a href="files"><i class="fas fa-file"></i>Files</a></li>
                <li><a href="#"><i class="fas fa-search"></i>Search</a></li>
                <li><a href="#"><i class="fas fa-cog"></i>Settings</a></li>
                <li><a href="main"><i class="fas fa-lock"></i>Log Out</a></li>
            </ul>
        
        </div>
        
        <div class="main_content">
            <form action = '' method = 'GET'>
            <div class="header">Welcome Search!!!</div>
            <div class="document">
            <label for="company">Company Name</label>
            <select id="company" name="company">
                <option value="VeeFish Nets">VeeFish Nets</option>
                <option value="Seba Nets">Seba Nets</option>
                <option value="Vee Nets">Vee Nets</option>
            </select>
            <br>  <label for="document">Document type</label>
            <select id="document" name="document">
                <option value="Invoice">Invoice</option>
                <option value="Purchase Orders">Purchase Orders</option>
                <option value="Shipment">Shipment</option>
            </select>
            <br> <label id = "name" for="key">Key value</label>
                <input type="text" name="key" placeholder="Key value">
            <br> <label for="cname">Customer Name</label>
            <input type="text" name="cname" placeholder="customer name">
            <br> <label for="cnumber">Customer Number</label>
            <input type="text" name="cnumber" placeholder="customer number">
            <br><label for="fdate">From Date</label>
            <input type="date" name="fromdate" id="fdate">
            <br><label for="tdate">To Date</label>
            <input type="date" name="todate" id="tdate">
            <br><label for="ponum">PO Number</label>
            <input type="text" name="ponum" placeholder="PO number">
            <br><label for="podate">PO Date</label>
            <input type="date" name="podate" placeholder="PO date">
            <br><label for="ordernum">Order Number</label>
            <input type="text" name="ordernum" placeholder="order number">
            <br><label for="shipdate">Shipment Date</label>
            <input type="date" name="shipdate" placeholder="Shipment Date">
            <br><label for="invnum">Invoice Number</label>
            <input type="text" name="invnum" placeholder="Invoice Number">
            <br><label for="orderdate">Order Date</label>
            <input type="date" name="orderdate" placeholder="Invoice Date">
            <br><input type="submit" name="" value="Search">
            <br><input type="reset" onclick = 'res()' class = "res" name="" value="Reset">
            </div>
            <table class="files" id = 'table' style = 'cursor: pointer;'>
                   <?php
                  
                        if($check > 0)
                        { 
                            $global = true;
                            if($document == 'Invoice')
                            {
                               echo '<tr><th>Id</th><th>Company</th><th>Document Class</th><th>Customer Name</th>
                                <th>Invoice Number</th><th>Purchase Number</th><th>Order date</th><th>Amount</th><th>Filename</th><th>Key</th></tr>';

                               while($row = mysqli_fetch_assoc($result))
                                    {
                                        echo "<tr onclick = 'tablevalues(this)'><td>".$row['id']."</td><td>".$row['company']."</td><td>" .$row['documentclass']."</td><td>".$row['customername']."</td><td>".$row['invoicenumber']."</td><td>".$row['purchasenumber']."</td><td>".$row['orderdate']."</td><td>".$row['amount']."</td><td>".$row['filename']."</td><td>".$row['keyvalues']."</td></tr>";
                                    }
                            }
                            else if($document == 'Shipment')
                            {
                               echo '<tr><th>Id</th><th>Company</th><th>Document class</th><th>Shipment date</th><th>Order Number</th><th>Invoice Number</th><th>Order date<th>Filename</th></th><th>Key</th></tr>';

                               while($row = mysqli_fetch_assoc($result))
                                    {
                                        echo "<tr onclick = 'tablevalues(this)'><td>".$row['id']."</td><td>".$row['company']."</td><td>" .$row['documentclass']."</td><td>".$row['shipmentdate']."</td><td>".$row['ordernumber']."</td><td>".$row['invoicenumber']."</td><td>".$row['orderdate']."</td><td>".$row['filename']."</td><td>".$row['keyvalues']."</td></tr>";
                                    }
                            }
                            else
                            {
                               echo '<tr><th>Id</th><th>Company</th><th>Document class</th><th>Customer Name</th><th>Customer Number</th><th>PO Number</th><th>PO date</th><th>Amount</th><th>Filename</th><th>Key</th></tr>';

                               while($row = mysqli_fetch_assoc($result))
                                    {
                                        echo "<tr onclick = 'tablevalues(this)'><td>".$row['id']."</td><td>".$row['company']."</td><td>" .$row['documentclass']."</td><td>".$row['customername']."</td><td>".$row['customernumber']."</td><td>".$row['ponumber']."</td><td>".$row['podate']."</td><td>".$row['amount']."</td><td>".$row['filename']."</td><td>".$row['keyvalues']."</td></tr>";
                                    }
                            }  
                        }

                    ?>
            </table>

            
        </div>
        </form>
    </div>
       <div class = "popup_upload1" id = "popupupload1" style = 'width : 1600px;'>
           <div class = "content2">
                <i class="fas fa-times"></i>
                <h4 style = "font-family:courier,arial,helvetica;margin : 15px;">Documents not available!</h4>
                <button onclick = 'call2()'>Ok</button>
           </div>
       </div> 
       <a href = '#table' id = 'anco' hidden></a>
    </body>
    
</html>

<style>
    .popup_upload1
    {
     <?php
      if($error)
       {
          echo 'display: flex';
       }
     ?>
    }
    .content2{
        <?php
            if($error)
            {
                echo "display: flex";
            }
        ?>
    }
</style>

<?php 
    if($check != 0)
    {
      echo "<script>document.getElementById('anco').click();</script>";
    }
?>
<script>
    function res()
    {
       window.location.replace('search');
    }
</script>