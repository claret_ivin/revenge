<?php 
    
    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';


    $conn = mysqli_connect($servername ,$username , $password , $database);

if(!empty($_POST['password']))
{
    $sql = "SELECT * FROM login";

    $result = mysqli_query($conn,$sql);

    $check = mysqli_num_rows($result);
    if($check > 0)
    {
        $name = $_POST['name'];
        $pass = $_POST['password'];
        
        while($row = mysqli_fetch_assoc($result))
        {
            if($name == $row['username'] & $pass == $row['password'])
            {
                session_start();
                $_SESSION['username'] = $name;
                $_SESSION['password'] = $pass;

                if($row['id'] == 2)
                {
                    $_SESSION['adminpassword'] = $row['password'];
                }
               
                header("Location: main?login=succeed");
            }
        }

        $error1 = '#worng username';
        $error2 = '#worng password';
   }
    else
    {
    echo 'something went wrong reload it!';
    }
}
?>


<!DOCTYPE html>

<html>
    <head>
        <title>Login From</title>
            <link rel="stylesheet" type="text/css" href="newcss.css">
             <script src="https://kit.fontawesome.com/a076d05399.js"></script>
             <script src="script.js"></script>
             <link rel="stylesheet" type="text/css" href="css/logincss.css">
             <link rel="shortcut icon" href="images/appletlogo.png">
    </head>
    <body>
        <div class="loginbox">
            <img src="images/avatar.png" class="avatar">
            <h1>Login Here</h1>
            <form  action = '' method = 'POST'>
                <p>UserName</p>
                <input type="text" name="name" placeholder="username" required="" autocomplete="off">
                <p style = 'font-size: 13px;color : red;margin-bottom : 10px;'><?php if(isset($error1)) echo $error1; ?></p>
                <p>Password</P>
                <input type="password" id = 'password' name="password" placeholder="password" required="">  
                 <i class="far fa-eye eyes" id="togglePassword"></i>
                 <p style = 'font-size: 13px;color : red;margin-bottom : 10px;'><?php if(isset($error2)) echo $error2; ?></p> 
                <input type="submit" name="" value="Login">
                <a href="#">Forgot Password</a><br>
            </form>
         </div>

         <script>
             const togglePassword = document.querySelector('#togglePassword');
             const password = document.querySelector('#password');

             togglePassword.addEventListener('click', function (e) {
             const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
             password.setAttribute('type', type);
             this.classList.toggle('fa-eye-slash');
});
         </script>
    </body>
</html> 