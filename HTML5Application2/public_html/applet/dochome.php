<?php 

 session_start();    

    if (!(isset($_SESSION['username'])))
    {
    
        echo '<link rel="shortcut icon" href="applet.jpg">';
        echo '<h1>Direct File Access is Prohibited</h1>';
        die(); 
    }

    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';
    $invoice = $purchase = $shipment = 0;

    $conn = mysqli_connect($servername ,$username , $password , $database);

    $sql = "SELECT documentclass FROM productdocuments;";
    $result = mysqli_query($conn,$sql);
        while( $row = mysqli_fetch_assoc($result))
        {
        
            if($row['documentclass'] == 'Invoice')
            {
                $invoice++;
            }
            else if($row['documentclass'] == 'Shipment')
            {
                $shipment++;
            }   
            else
            {
                $purchase++;
            }                
        }
    $sql = "SELECT id,company,documentclass,filename,dates FROM productdocuments WHERE id ORDER BY id DESC LIMIT 12;";
    $result = mysqli_query($conn,$sql);
    $checks = mysqli_num_rows($result);
    
?>

<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" type="text/css" href="css/dochomecs.css">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="scripts/script.js"></script>
        <link rel="shortcut icon" href="images/appletlogo.png">
    </head>
    <body>
        <form>
     
           
    <div class="wrapper">  
        <div class="sidebar">
            <header>Product Details</header>
            <ul>
                <li><a href="#"><i class="fas fa-home"></i>Home</a></li>
                <li><a href="files"><i class="fas fa-file"></i>Files</a></li>
                <li><a href="search"><i class="fas fa-search"></i>Search</a></li>
                <li><a href="#"><i class="fas fa-cog"></i>Settings</a></li>
                <li><a href="main"><i class="fas fa-lock"></i>Log Out</a></li>
            </ul>
        </div>
       
        <div class="main_content">
            <div class="header">
                <p>Welcome Home!!!</p>
               
            </div>
           <div style = "display : flex;">
            <div class="container1">
                <br> <div class="box1">
                        <ul>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i>More Info</a>
                            <div class="submenu">
                                    <a href="#" onclick = "shortcutsearch('VeeFish Nets','Invoice')">VeeFish Nets</a>
                                    <a href="#" onclick = "shortcutsearch('Seba Nets','Invoice')">Seba Nets</a>
                                    <a href="#" onclick = "shortcutsearch('Vee Nets','Invoice')">Vee Nets</a>
                            </div>
                            </li>
                            <h3 style = 'color : white;margin :15px;font-family:arial,helvetica;'>Invoice</h3>
                            <h1 style = 'color : white;margin-left : 40px;'><?php echo $invoice; ?></h1>
                        </ul>
                 </div>
                <br>  <div class="box2">
                        <ul>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i>More Info</a>
                                <div class="submenu" style = 'margin-left : 200px;'>
                                    <a href="#" onclick = "shortcutsearch('VeeFish Nets','Purchase Orders')">VeeFish Nets</a>
                                    <a href="#" onclick = "shortcutsearch('Seba Nets','Purchase Orders')">Seba Nets</a>
                                    <a href="#" onclick = "shortcutsearch('Vee Nets','Purchase Orders')">Vee Nets</a>
                                </div>
                            </li>
                            <h3 style = 'color : white;margin :15px;font-family:arial,helvetica;'>Purchase orders</h3>
                            <h1 style = 'color : white;margin-left : 40px;'><?php echo $purchase; ?></h1>
                        </ul>
                 </div>
                <br>   <div class="box3">
                        <ul>
                            <li><a href="#"><i class="fas fa-angle-double-right"></i>More Info</a>
                                <div class="submenu">
                                    <a href="#" onclick = "shortcutsearch('VeeFish Nets','Shipment')">VeeFish Nets</a>
                                    <a href="#" onclick = "shortcutsearch('Seba Nets','Shipment')">Seba Nets</a>
                                    <a href="#" onclick = "shortcutsearch('Vee Nets','Shipment')">Vee Nets</a>
                                </div>
                            </li>
                            <h3 style = 'color : white;margin :15px;font-family:arial,helvetica;'>Shipments</h3>
                            <h1 style = 'color : white;margin-left : 40px;'><?php echo $shipment; ?></h1>
                        </ul>
                 </div>
                 
            </div>
             
               <table class="recent" style = 'cursor: pointer;'>
                  <tr>
                    <th>Id</th>
                    <th>Company</th>
                    <th>Document class</th>
                    <th>Filename</th>
                    <th>Stored date</th>
                  </tr>
  
                 
                 <?php
                    if(isset($checks) > 0)
                    { 
                      while($row = mysqli_fetch_assoc($result))
                      {
                          echo "<tr onclick = 'tablevalues(this)'><td>".$row['id']."</td><td>" .$row['company']."</td><td>".$row['documentclass']."</td><td>".$row['filename']."</td><td>".$row['dates']."</td></tr>";
                      }
                    }
                ?>
                </table>
           </div>
            <div class="container3">
                <a href="#" onclick = 'shortcutfiles("VeeFish Nets")'><i class="fas fa-arrow-circle-right"></i>Vee Fish Nets</a>
                <a href="#" onclick = 'shortcutfiles("Seba Nets")'><i class="fas fa-arrow-circle-right"></i>Seba Nets</a>
                <a href="#" onclick = 'shortcutfiles("Vee Nets")'><i class="fas fa-arrow-circle-right"></i>Vee Nets</a>
            </div>
           
           
        </div>
       
    </div>
       
        </form>    

        </body>
</html>

<script>

function tablevalues(val)
{
    document.cookie = "id = "+ val.childNodes[0].innerHTML;
    document.cookie = "company = "+ val.childNodes[1].innerHTML;
    document.cookie = "document = "+ val.childNodes[2].innerHTML;
    var val = "<?php  $_SESSION['home'] = 'yes'; ?>";
    window.location.href = 'homesearch.php';
}

</script>
