<?php
  
    session_start();    

    if (!(isset($_SESSION['username'])))
    {
    
        echo '<link rel="shortcut icon" href="applet.jpg">';
        echo '<h1>Direct File Access is Prohibited</h1>';
        die(); 
    }

    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';

    $conn = mysqli_connect($servername ,$username , $password , $database);

 function resizeImage($image,$image_width,$image_height,$imagename) 
    {
       /* $ratio = ($image_width+$image_height)/900;
        $resizeWidth = $image_width/$ratio;
        $resizeHeight = $image_height/$ratio; */

         $resizeWidth = 450;
         $resizeHeight = 350;

        $file = imagecreatefromstring(file_get_contents($image));
        $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
        imagecopyresampled($imageLayer,$file,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
        imagepng($imageLayer,$image);

        move_uploaded_file($image,$imagename);

    }

 if($_SERVER['REQUEST_METHOD']=='POST')
 {
    $sql = "UPDATE staff_documents SET"; 
    if(!empty($_POST['id1']))
    {
        $sql = $sql. " staffid = '".$_POST['id1']."'";
    }
    if(!empty($_POST['name1']))
    {
        $sql = $sql. ", staffname = '".$_POST['name1']."'";   
    }
    if(!empty($_POST['gender1']))
    {
        $sql = $sql. ", gender = '".$_POST['gender1']."'";   
    }
    if(!empty($_FILES['photo']["name"]))
    {
        $photo = $_FILES['photo']['tmp_name']; 
        $bytesize = filesize($photo)/1024;
        $size = getimagesize($photo);
        if($bytesize > 250)
        {
            resizeImage($photo,$size[0],$size[1],$_FILES['photo']['name']);
            $file1 = addslashes(file_get_contents($_FILES['photo']['name']));
        }
        else
        {
            $file1 = addslashes(file_get_contents($_FILES['photo']['tmp_name']));
        }
        $sql = $sql . ",photo = '".$file1."'"; 
    }
    if(!empty($_FILES['aadhar']["name"]))
    {
        $aadhar = $_FILES['aadhar']['tmp_name']; 
        $bytesize = filesize($aadhar)/1024;
        $size = getimagesize($aadhar);
        if($bytesize > 250)
        {
            resizeImage($aadhar,$size[0],$size[1],$_FILES['aadhar']['name']);
            $file2 = addslashes(file_get_contents($_FILES['aadhar']['name']));
        }
        else
        {
            $file2 = addslashes(file_get_contents($_FILES['aadhar']['tmp_name']));
        }
        $sql = $sql . ",aadhar = '".$file2."'"; 
    }
    if(!empty($_FILES['license']["name"]))
    {
        $license = $_FILES['license']['tmp_name']; 
        $bytesize = filesize($license)/1024;
        $size = getimagesize($license);
        if($bytesize > 250)
        {
            resizeImage($license,$size[0],$size[1],$_FILES['license']['name']);
            $file3 = addslashes(file_get_contents($_FILES['license']['name']));
        }
        else
        {
            $file3 = addslashes(file_get_contents($_FILES['license']['tmp_name']));
        }
        $sql = $sql . ",license = '".$file3."'"; 
    }
    if(!empty($_FILES['other']["name"]))
    {
        $other = $_FILES['other']['tmp_name']; 
        $bytesize = filesize($other)/1024;
        $size = getimagesize($other);
        if($bytesize > 250)
        {
            resizeImage($other,$size[0],$size[1],$_FILES['other']['name']);
            $file4 = addslashes(file_get_contents($_FILES['other']['name']));
        }
        else
        {
            $file4 = addslashes(file_get_contents($_FILES['other']['tmp_name']));
        }
        $sql = $sql . ",other = '".$file4."'"; 
    }
     $sql = $sql. " WHERE staffid = '".$_COOKIE['staffid']."' AND staffname = '".$_COOKIE['staffname']."';"; 
     

     $result = mysqli_query($conn,$sql);

     if (file_exists($_FILES['photo']['name']))
    {
        unlink($_FILES['photo']['name']);
    }
    if (file_exists($_FILES['aadhar']['name']))
    {
        unlink($_FILES['aadhar']['name']);
    }
    if (file_exists($_FILES['license']['name']))
    {
        unlink($_FILES['license']['name']);
    }
    if (file_exists($_FILES['other']['name']))
    {
        unlink($_FILES['other']['name']);
    }

     header("Location:../staffsearch?id=".$_POST['id1']."&name=".$_POST['name1'].""); 
}
else
{
    header("Location:../staffsearch");
}
?> 

