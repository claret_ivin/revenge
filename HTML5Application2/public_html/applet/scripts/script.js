
function shortcutsearch(cmpy,doc)
{
  window.location.href = 'search?company='+cmpy+'&document='+doc+'#table';
}

function shortcutfiles(val)
{
    localStorage.setItem("search", val);
    window.location.href = 'files';
}

// ===============================================================//

function fileload()
{
  if(localStorage.getItem("search"))
  {
     document.getElementById("company").value = localStorage.getItem("search");
     localStorage.removeItem("search");
  }
}


function documentclass()
{
    var documents = document.getElementById("document").value;
    document.getElementById("reset").click();
    if(documents == "Invoice")
    {
        document.getElementById("document").value = "Invoice";
        document.getElementById("name").innerHTML = "Customer Name"
        document.getElementById("number").innerHTML = "Invoice Number"
        document.getElementById("date").innerHTML = "Order Date"
        document.getElementById("num").innerHTML = "Purchase Number"
        document.getElementById("amt").style.display = "flex";
        document.getElementById("amtbox").style.display = "flex";
        document.getElementById("box1").placeholder = "Customer Name";
        document.getElementById("inumbox").placeholder = "Invoice Number";
        document.getElementById("box2").placeholder = "Purchase Number";
        document.getElementById("box1").type = "text";
    }
    else if(documents == "Shipment")
    {
        document.getElementById("document").value = "Shipment";
        document.getElementById("name").innerHTML = "shipment Date";
        document.getElementById("number").innerHTML = "Invoice Number";
        document.getElementById("date").innerHTML = "Order Date";
        document.getElementById("num").innerHTML = "Order Number";
        document.getElementById("amt").style.display = "none";
        document.getElementById("amtbox").style.display = "none";
        document.getElementById("inumbox").placeholder = "Invoice Number";
        document.getElementById("box2").placeholder = "Order Number";
        document.getElementById("box1").type = "date";
    }
    else{
        document.getElementById("document").value = "Purchase Orders"
        document.getElementById("name").innerHTML = "Customer Name"
        document.getElementById("number").innerHTML = "Customer Number"
        document.getElementById("date").innerHTML = "PO Date"
        document.getElementById("num").innerHTML = "PO Number"
        document.getElementById("amt").style.display = "flex";
        document.getElementById("amtbox").style.display = "flex";
        document.getElementById("box1").placeholder = "Customer Name";
        document.getElementById("inumbox").placeholder = "Customer Number";
        document.getElementById("box2").placeholder = "PO Number";
        document.getElementById("box1").type = "text";
    }
}

//  ------------------------------------ pdfview ---------------------------------------------

function pdfupdate()
{
    document.getElementById("frame").style.width = "950px";
    document.getElementById("frame").style.height = "660px";
    document.getElementById("editindex").style.display = "block";
}

function pdfupdateclose()
{
    document.getElementById("frame").style.width = "1300px";
    document.getElementById("frame").style.height = "1000px";
    document.getElementById("editindex").style.display = "none";
}

function downloadpdf()
{
  document.getElementById("pdfdown").click();
}

 function pdfupdateclose()
    {
    document.getElementById("frame").style.width = "1300px";
    document.getElementById("frame").style.height = "1000px";
    document.getElementById("editindex").style.display = "none";
    }