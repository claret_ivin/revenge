<?php
  
    session_start();    

    if (!(isset($_SESSION['username'])))
    {
      die('<h1>Direct File Access Prohibited</h1>');
    }

    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';

    $global = false;
    $finalmsg;

    $aadhar = 'null';
    $license = 'null';
    $other = 'null';
    $conn = mysqli_connect($servername ,$username , $password , $database);

    function resizeImage($image,$image_width,$image_height,$imagename) 
    {
       /* $ratio = ($image_width+$image_height)/900;
        $resizeWidth = $image_width/$ratio;
        $resizeHeight = $image_height/$ratio; */

         $resizeWidth = 450;
         $resizeHeight = 350;

        $file = imagecreatefromstring(file_get_contents($image));
        $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
        imagecopyresampled($imageLayer,$file,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
        imagepng($imageLayer,$image);

        move_uploaded_file($image,$imagename);

    }

    if(isset($_POST['id']) || isset($_POST['name']))
    {

     $id = $_POST['id'];
     $name = $_POST['name'];
     $gender = $_POST['gender'];

     $photo = $_FILES['photo']['tmp_name']; 
     $bytesize = filesize($photo)/1024;
     $size = getimagesize($photo);
     if($bytesize > 250)
     {
       resizeImage($photo,$size[0],$size[1],$_FILES['photo']['name']);
       $photo = addslashes(file_get_contents($_FILES['photo']['name']));

     }
     else
     {
        $photo = addslashes(file_get_contents($_FILES['photo']['tmp_name']));
     }

     $sql = "SELECT staffid FROM staff_documents WHERE staffid = '".$id."'";

      $result = mysqli_query($conn,$sql);
      $check = mysqli_num_rows($result);
           if($check > 0)
           {
               $global = true;
               $finalmsg = "error";
               $msg = 'StaffID is already exist!';
           }
           else
           {
               
             $sql = "INSERT INTO staff_documents (staffid , staffname,gender ,photo) VALUES( '".$id."' , '".$name."' ,'".$gender."', '".$photo."');";


             if(!empty($_FILES["aadhar"]["name"]))
             {
                $aadhar = $_FILES['aadhar']['tmp_name']; 
                $bytesize = filesize($aadhar)/1024;
                $size = getimagesize($aadhar);
                if($bytesize > 250)
                {
                   resizeImage($aadhar,$size[0],$size[1],$_FILES['aadhar']['name']);
                   $aadhar = addslashes(file_get_contents($_FILES['aadhar']['name']));

                }
                else
                {
                    $aadhar = addslashes(file_get_contents($_FILES['aadhar']['tmp_name']));
                }
                $sql = "INSERT INTO staff_documents (staffid , staffname,gender ,photo,aadhar) VALUES( '".$id."' , '".$name."' ,'".$gender."', '".$photo."' , '".$aadhar."' );";
             }
             if(!empty($_FILES["license"]["name"]))
             {
                $license = $_FILES['license']['tmp_name']; 
                $bytesize = filesize($license)/1024;
                $size = getimagesize($license);
                if($bytesize > 250)
                {
                   resizeImage($license,$size[0],$size[1],$_FILES['license']['name']);
                   $license = addslashes(file_get_contents($_FILES['license']['name']));
                }
                else
                {
                    $license = addslashes(file_get_contents($_FILES['license']['tmp_name']));
                }
                $sql = "INSERT INTO staff_documents (staffid , staffname,gender ,photo,aadhar,license) VALUES( '".$id."' , '".$name."' ,'".$gender."', '".$photo."' , '".$aadhar."' , '".$license."' );";
             }
             if(!empty($_FILES["other"]["name"]))
             {
                $other = $_FILES['other']['tmp_name']; 
                $bytesize = filesize($other)/1024;
                $size = getimagesize($other);
                if($bytesize > 250)
                {
                   resizeImage($other,$size[0],$size[1],$_FILES['other']['name']);
                   $other = addslashes(file_get_contents($_FILES['other']['name']));
                }
                else
                {
                    $other = addslashes(file_get_contents($_FILES['other']['tmp_name']));
                }
                $sql = "INSERT INTO staff_documents (staffid , staffname,gender ,photo,aadhar,license,other) VALUES( '".$id."' , '".$name."' ,'".$gender."', '".$photo."' , '".$aadhar."' , '".$license."' , '".$other."' );";
             }
             
            $result = mysqli_query($conn,$sql);
            if($result)
             {
                     $global = true;
                     $finalmsg = "success";
                     if (file_exists($_FILES['photo']['name']))
                     {
                       unlink($_FILES['photo']['name']);
                     }
                     if (file_exists($_FILES['aadhar']['name']))
                     {
                       unlink($_FILES['aadhar']['name']);
                     }
                     if (file_exists($_FILES['license']['name']))
                     {
                       unlink($_FILES['license']['name']);
                     }
                     if (file_exists($_FILES['other']['name']))
                     {
                       unlink($_FILES['other']['name']);
                     }
             }
            else
            {
                     $global = true;
                     $finalmsg = "error";
                     $msg = "Image size is too long!";
                     if (file_exists($_FILES['photo']['name']))
                     {
                       unlink($_FILES['photo']['name']);
                     }
                     if (file_exists($_FILES['aadhar']['name']))
                     {
                       unlink($_FILES['aadhar']['name']);
                     }
                     if (file_exists($_FILES['license']['name']))
                     {
                       unlink($_FILES['license']['name']);
                     }
                     if (file_exists($_FILES['other']['name']))
                     {
                       unlink($_FILES['other']['name']);
                     }
            } 
          }
    }

?>

<!DOCTYPE html>
<html>
    <head>

        <title>Staff Documents | Upload</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/staff_css.css">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="scripts/script2.js"></script>
        <link rel="shortcut icon" href="images/appletlogo.png">
    </head>
    <body>
        <div class="sidenav">
            <h3 class = "header">Staff Documents</h3>
            <img src="images/users-group.png" class="avatar">
            <div class = "sidenav2">
                <i class="fas fa-home"></i>
                <a class = "side1" href="staff-home">Home</a><br>
            </div>
            <div class = "sidenav2">
                <i class="fas fa-folder"></i>
                <a href="#">Upload Documents</a><br>
            </div>
            <div class = "sidenav2">
                <i class="fas fa-search"></i>
                <a href="staffsearch">Search Documents</a><br>
            </div>
            <div class = "sidenav2">
                <i class="fas fa-lock"></i>
                <a class = "side1" href="main">Logout</a><br>
            </div>
        </div>


        <header style = 'position: static; width : 100%;'>
            <div class = "navbar" style = 'justify-content : center;'>
                 <h4 style ='color:silver;'>Welcome user!</h4>
            </div>
        </header>

        <div class = "container">
            <div class = "split1">
                <form action =  'staffupload' method = POST enctype="multipart/form-data">
                    <label>Staff Id</label>
                    <input type="text" name = "id" placeholder="Id" autocomplete="off" required = ""><br>
                    <label>Staff Name</label>
                    <input type="text" name = "name" placeholder="Name" autocomplete="off" required = ""><br>
                    <label>Gender</label>
                    <div style = 'display : flex; ' >
                        <label style = 'margin-left : 250px;'>Male</label>
                        <input type = 'radio' value = 'male' name = 'gender' required="" style = 'width : 20px;height : 20px; border : none;'>
                        <label style = 'margin-left : 490px;'>Female</label> 
                        <input type = 'radio' value = 'female' name = 'gender' style = 'width : 20px;height : 20px; margin-left : 200px;outline : none;'><br>
                    </div>
                    <div class = "choosefile"> 
                        <input id = "file1" type="file" name = "photo" accept="image/*"
                         onchange="preview(window.URL.createObjectURL(this.files[0]),'file1','pre1')" required= ""><br>
                        <input id = "file2" type="file" name = "aadhar" accept="image/*"
                        onchange="preview(window.URL.createObjectURL(this.files[0]),'file2','pre2')"><br>
                        <input id = "file3" type="file" name = "license" accept="image/*"
                        onchange="preview(window.URL.createObjectURL(this.files[0]),'file3','pre3')"><br>
                        <input id = "file4" type="file" name = "other" accept="image/*"
                        onchange="preview(window.URL.createObjectURL(this.files[0]),'file4','pre4')"><br>
                    </div>
                    <label>Photo</label>
                    <input id = "pre1" onfocus = "choosefile(this,'file1')" type="text" name = "pho_to" placeholder="Photo" autocomplete="off" required= ""><br>
                    <label>Aadhar</label>
                    <input id = "pre2" onfocus = "choosefile(this,'file2')" type="text" name = "aad_har" placeholder="Aadhar" autocomplete="off"><br>
                    <label>Driving License</label>
                    <input id = "pre3" onfocus = "choosefile(this,'file3')" type="text" name = "lice_nse" placeholder="Driving License" autocomplete="off"><br>
                    <label>Other </label>
                    <input id = "pre4" onfocus = "choosefile(this,'file4')" type="text" name = "oth_er" placeholder="Other" autocomplete="off"><br>

                     <button type = "submit" class = "button1">Submit</button>
                    <button type = "reset" class = "button2" onclick = "document.getElementById('output').src = ''">Reset</button>
                </form>
               
            </div> 
            <div class = "split2">
                
                <div class = "box">
                       <img id="output" class="image"/>
                </div>
            </div>
        </div>
        
        <div class = "popup_upload1" id = "popupupload1">
            <div class = "content">
                <i class="fas fa-check"></i>
                <h4 style = "font-family:courier,arial,helvetica; margin : 15px;">successfully submitted!</h4>
                <button onclick = 'call2()'>Ok</button>
            </div>
            <div class = "content2">
                <i class="fas fa-times"></i>
                <h4 style = "font-family:courier,arial,helvetica;margin : 15px;"><?php echo $msg ?></h4>
                <button onclick = 'call2()'>Ok</button>
            </div>
        </div>
       
    </body>
</html>


<style>
    .popup_upload1
    {
     <?php
      if($global == false)
       {
          echo 'display: none';
       }
       else
       {
          echo 'display:flex';
       }
     ?>
    }

    .content
    {
      <?php
      if($finalmsg == "success")
         {
            echo 'display:flex';
         }
       ?>
    }

    .content2
    {
      <?php
        if($finalmsg == "error")
        {
           echo 'display:flex';
        }
       ?>
    }

</style>
