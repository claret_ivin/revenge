<?php
  
    session_start();    

    if (!(isset($_SESSION['username'])))
    {
      die('<h1>Direct File Access Prohibited</h1>');
    }


    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';

    $male = 0;
    $female = 0;

    $conn = mysqli_connect($servername ,$username , $password , $database);
    
    $sql = "SELECT gender FROM staff_documents;";

    $result = mysqli_query($conn,$sql);

    $check = mysqli_num_rows($result);

        while( $row = mysqli_fetch_assoc($result))
        {
        
            if($row['gender'] == 'male')
            {
                $male++;
            }
            else
            {
                $female++;
            }                   
        }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" type="text/css" href="css/staff_css.css"> 
        <script src="https://kit.fontawesome.com/a076d05399.js"></script> 
        <link rel="shortcut icon" href="images/appletlogo.png">
    </head>
    <body>
        <div class = "navbar" style = "left : 0; background : linear-gradient(120deg,#2c3e50,#3498db
        ); height : 100px;">
            <img src = "images/appletlogo.png">
            <h3 style = 'color : white;font-family: Georgia;margin : 20px;'>APPLET<br><span style = 'margin : 10px;font-size:10px'>-εκδίκηση-</span></h3>
            <h4 style = 'color : white;margin-left : 460px;'>Welcome Home</h4>
            <div class = 'dropdown' style = ' position : absolute; right : 50px;padding:30px 0px 30px 0px;'>
                <img src = "images/user.jpg">
                <div class = 'dropdown-content'>
                    <a  href = 'main' style = 'position: fixed;right : 50px;top : 100px;border-radius: 3px; padding :10px 17px 10px 17px;background: lightgray;color : black;font-size: 17px;text-decoration: none;'>Logout</a>
                </div>
            </div>
        </div>

        <!--- container ---->

        <div style = "width: 100%; max-height : 100%;position: relative;top : 20px;display : flex;">


            <div style = "width : 900px;height: 590px;display : flex;align-items: center;flex-direction: column;">
               <div style = "width : 800px; height :260px;border-radius : 3px 3px 0px 0px; background : rgb(59, 168, 211);display : flex;justify-content: center;">
                    <span>
                        <div style = "width : 700px; height :470px; border-radius: 10px;background : white; box-shadow: 3px 3px 15px lightgray;position : relative;top : 60px;display : flex;flex-direction: row;">
                            <div class = "uploadcontainer">
                                <i class = "fa fa-upload"></i>
                                <h3 style = "font-family:arial,helvetica;color :rgb(5, 73, 100);margin : 5px;">Upload Documents</h3>
                                <div style = "  border-bottom: 1px solid;width: 230px;border-color: rgb(59, 168, 211,0.5);"></div>
                                <form>
                                    <button style = 'outline:none;cursor: pointer;' formaction="staffupload">Open</button>
                                </form>
                            </div>
                            <div style = "  border-left: 1px solid;height: 400px;margin-top: 35px;border-color: rgb(59, 168, 211,0.5);"></div>
                            <div class = "searchcontainer2">
                                <i class="fas fa-search"></i>
                                <h3 style = "font-family:arial,helvetica;color :rgb(5, 73, 100);margin : 5px;">Search Documents</h3>
                                <div style = "  border-bottom: 1px solid;width: 230px;border-color: rgb(59, 168, 211,0.5);"></div>
                                <form>
                                    <button style = 'outline:none;cursor: pointer' formaction="staffsearch">Open</button>
                                </form>
                            </div>
                        </div>
                    </span>
                </div>
                <div style = "width : 800px; height :300px; background : rgb(59, 168, 211,0.1);display : flex;justify-content :space-around;">
                </div>
            </div>

            <div style = "width : 650px;height: 570px;margin-top : 20px; display: flex; justify-content: center;">
                <div id="piechart"></div>

                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        
                        <script type="text/javascript">
                        // Load google charts
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(drawChart);
                        
                        // Draw the chart and set the chart values
                        function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                        ['staffs', 'total'],
                        ['Male', <?php echo $male ?>],
                        ['Female', <?php echo $female ?>],
                        ]);
                        
                        // Optional; add a title and set the width and height of the chart
                        var options = {'title':'Staffs', 'width':550, 'height':400};
                        
                        // Display the chart inside the <div> element with id="piechart"
                        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                        chart.draw(data, options);
                        }
                        </script>
                </div>
            </div>

        </div>

    </body>
</html>

