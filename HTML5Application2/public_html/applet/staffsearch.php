<?php
  
    session_start();    

    if (!(isset($_SESSION['username'])))
    {
    
        echo '<link rel="shortcut icon" href="applet.jpg">';
        echo '<h1>Direct File Access is Prohibited</h1>';
        die(); 
    }

    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';
    $global = false;
    $male = 0;
    $female = 0;

    $conn = mysqli_connect($servername ,$username , $password , $database);

    $sql = "SELECT gender FROM staff_documents;";
    $result = mysqli_query($conn,$sql);
        while( $row = mysqli_fetch_assoc($result))
        {
        
            if($row['gender'] == 'male')
            {
                $male++;
            }
            else
            {
                $female++;
            }                   
        }

    if(isset($_GET['id']) || isset($_GET['name']))
    {
        if(!empty($_GET['id']) || !empty($_GET['name']))
        {
            $id = $_GET['id'];
            $name = $_GET['name'];

              $sql = "(SELECT staffname,staffid,photo,aadhar,license,other,gender FROM staff_documents WHERE staffid ='". $id ."' AND staffname='".$name."')";

            if($id == '')
            {
                  $sql = "(SELECT staffname,staffid,photo,aadhar,license,other,gender FROM staff_documents WHERE staffname = '". $name ."' )";
            }
            if($name == '')
            {
                  $sql = "(SELECT staffname,staffid,photo,aadhar,license,other,gender FROM staff_documents WHERE staffid = '". $id ."' )";
            }

            $result = mysqli_query($conn,$sql);

            $check = mysqli_num_rows($result);
            if($check > 0)
            { 
                $row = mysqli_fetch_assoc($result);
                $photo = $row['photo'];
                $aadhar = $row['aadhar'];
                $license = $row['license'];
                $other = $row['other'];  
                $name = $row['staffname']; 
                $id = $row['staffid'];
                $gender = $row['gender'];

                $global = true;

                setcookie("staffid" , $id);
                setcookie("staffname" , $name);
                $_SESSION['staffname'] = $name;
            }

            else
            {
                $global = false;
                $error = true;
                unset($_SESSION["staffname"]);
            }
        }
        else{

            unset($_SESSION["staffname"]);
            $sql = "(SELECT * FROM staff_documents)";
            $result = mysqli_query($conn,$sql);
            $checks = mysqli_num_rows($result);
        }
    }
    else{
        unset($_SESSION["staffname"]);
    }    
?> 

<!DOCTYPE html>
<html>
    <head>

        <title>Staff Documents | search</title>
        <link rel="stylesheet" type="text/css" href="css/staff_css.css">
        <link rel="stylesheet" type="text/css" href="css/search.css">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="scripts/script2.js"></script>
        <link rel="shortcut icon" href="images/appletlogo.png">
    </head>
    <body>
        <div class="sidenav">
            <h3 class = "header">Staff Documents</h3>
            <img src="images/users-group.png" class="avatar">
            <div class = "sidenav2">
                <i class="fas fa-home"></i>
                <a class = "side1" href="staff-home">Home</a><br>
            </div>
            <div class = "sidenav2">
                <i class="fas fa-folder"></i>
                <a href="staffupload">Upload Documents</a><br>
            </div>
            <div class = "sidenav2">
                <i class="fas fa-search"></i>
                <a href="#">Search Documents</a><br>
            </div>
            <div class = "sidenav2">
                <i class="fas fa-lock"></i>
                <a class = "side1" href="main">Logout</a><br>
            </div>
        </div>
        
        <header style = 'position: static; top : 0px; width : 100%;'>
            <div class = "navbar" style = 'justify-content : center;'>
                 <h4 style ='color:silver;'>Welcome user!</h4>
            </div>
        </header>

        <div class = "searchcontainer">
            
            <div class = "searchsplit">
                <form action = 'staffsearch' method = 'GET' >
                    <label>Staff Id</label>
                    <input type="text" name = "id" id = 'id' placeholder="Id" autocomplete="off" value = "<?php if(isset($id)){echo $id;} ?>"><br>
                    <label>Staff Name</label>
                    <input type="text" name = "name" id = 'name' placeholder="Name" autocomplete="off" value = "<?php if(isset($name)){echo $name;} ?>"><br>
                    <label>Gender</label>
                    <input id = 'gendertext' type="text" placeholder="Gender" autocomplete="off" value = "<?php if(isset($gender)){echo $gender;} ?>" readonly><br>
                    <div id = 'genderradio' style = 'display: none' >
                        <input type = 'radio' value = 'male' name = 'gender' style = 'width : 20px;height : 20px; border : none;'>
                        <label style = 'margin-left : 260px; margin-top : 8px;'>Male</label>
                        <input type = 'radio' value = 'female' name = 'gender' style = 'width : 20px;height : 20px; margin-left : 230px;outline : none;'>
                        <label style = 'margin-left :260px;margin-top : 8px;'>Female</label> 
                    </div>
                    <div class = "searchbutton"> 
                        <button type = 'submit' id = 'formsubmit'>Search</button>
                        <button onclick ='res()' type ="reset" id = 'formreset'>Reset</button>
                    </div>
                </form>
            </div>


            <div class = "totallist">
                <div style = "display : flex;">
                    <h5>Staffs</h5>
                    <i class="fas fa-ellipsis-h totallisti" onclick = "optionopen()"></i>
                </div>
                <hr style="border: 1px thin;margin-left:15px; width: 270px;"/>
                <div style = "display : flex;">
                     <div style = "display : flex; justify-content : center; align-items : center; width : 130px;">
                        <div class="pie"><span class="tooltiptext"><i class="fas fa-male" style ="padding : 0px 12px 0px 12px;"></i><?php echo $male;?><i class="fas fa-female" style ="padding : 0px 10px 0px 10px;"></i><?php echo $female;?></span></div>

                        <script>
                            document.querySelector(".pie").style.backgroundImage =  "conic-gradient(rgb(86, 213, 252) 180deg ,  rgb(96, 252, 96) 180deg)";
                        </script>

                    </div>
                     <div style = "display : flex; flex-direction: column;">
                        <label><?php echo $male+$female; ?></label><br>
                        <i class="fas fa-male malei"></i>
                        <label style = "font-size : 20px;padding-left : 75px;  color : rgb(90, 89, 89);"><?php echo $male;?></label><br>
                        <i class="fas fa-female femalei"></i>
                        <label style = "font-size : 20px;padding-left : 75px; color : rgb(90, 89, 89);"><?php echo $female;?></label><br>
                     </div>
                </div>
            </div>
            <div class = "options">
                <div style = "display : flex;">
                    <h5>options</h5>
                    <i class="fa fa-times conicon" onclick = "optionclose()"></i>
                </div>
                <hr style="border: 1px thin;margin-left:15px; width: 270px;"/>
                <div style = "display : flex;flex-direction: column; justify-content: flex-start;">
                    <div class = "updateoption" id = 'update' style = 'margin-top : 10px;'>
                        <button onclick = "updateconfirm()">Confirm</button><div style = "  border-left: 1px solid;height: 40px;"></div><button onclick="updatecancel()">Cancel</button>
                    </div>
                    <button style = 'margin-top : 10px;' onclick = 'update()' class = 'update'>Update</button>
                    <hr style="border: 1px thin;height :0px; width: 300px;margin : 0px;"/>
                    <div class = "downloadoption">
                        <button onclick = "downloadconfirm()">Confirm</button><div style = "  border-left: 1px solid;height: 40px;"></div><button onclick="downloadcancel()">Cancel</button>
                    </div>
                    <button class = "disablebutton" onclick = "download()">Download</button>
                    <hr style="border: 1px thin; width: 300px;margin : 0px;"/>
                    <button onclick = "deletes()">Delete</button>
                </div>
            </div>

          <table class="files" id = 'table' style = 'cursor: pointer;'>
                   <?php
                  
                        if(isset($checks) > 0)
                        { 
                            $i = 1;
                            echo '<tr><th>SNo</th><th>Staff ID</th><th>Staff name</th></tr>';

                            while($row = mysqli_fetch_assoc($result))
                            {
                                echo "<tr onclick = 'tablevalues(this)'><td>".$i++."</td><td>" .$row['staffid']."</td><td>".$row['staffname']."</td></tr>";
                            }
                        }
                    ?>
            </table>

            <div class = 'box2' id = 'box2'>
                <img id = "1"src="data:image/jpg:chasrset-utf8;base64,<?php echo base64_encode($photo);?>" alt = 'None' onclick = "view(src,'1','a','file1')">
                <img id = "2" src="data:image/jpg:chasrset-utf8;base64,<?php echo base64_encode($aadhar);?>" alt = 'None' onclick = "view(src,'2','b','file2')">
                <img id = "3" src="data:image/jpg:chasrset-utf8;base64,<?php echo base64_encode($license);?>" alt = 'None' onclick = "view(src,'3','c','file3')">
                <img id = "4" src="data:image/jpg:chasrset-utf8;base64,<?php echo base64_encode($other);?>" alt = 'None' onclick = "view(src,'4','d','file4')">
            </div>
        </div>
        <div class = "popup2" id = "popup2">
            <img id="view" onclick = "view2()">
        </div> 
        <form>
            <div class = "check">
                <input id = "a" type="checkbox">
                <input id = "b" type="checkbox">
                <input id = "c" type="checkbox">
                <input id = "d" type="checkbox">
                <input type = "reset" id = "e" style = "display : none;">
            </div>
        </form>
        
        <div class = "popup_upload1" id = "popupupload1">
            <div class = "content2">
                <i class="fas fa-times"></i>
                <h4 style = "font-family:courier,arial,helvetica;margin : 15px;">Unknown staff id or name!</h4>
                <button onclick = 'call2()'>Ok</button>
            </div>
        </div>


        <div hidden>
            <a id = "phodown" href = "data:image/jpg:chasrset-utf8;base64,<?php echo base64_encode($photo);?>" download = "<?php echo $name.'-photo.png' ?>"></a>
            <a id = "aadhdown" href = "data:image/jpg:chasrset-utf8;base64,<?php echo base64_encode($aadhar);?>" download = "<?php echo $name.'-aadhar.png' ?>"></a>
            <a id = "licdown" href = "data:image/jpg:chasrset-utf8;base64,<?php echo base64_encode($license);?>" download = "<?php echo $name.'-license.png' ?>"></a>
            <a id = "otherdown" href = "data:image/jpg:chasrset-utf8;base64,<?php echo base64_encode($other);?>" download = "<?php echo $name.'-other.png' ?>"></a>
        </div>
        <div hidden>
            <form action = 'update/staffupdate.php' method = 'POST' enctype="multipart/form-data">
                <input type="text" name = "id1" id = 'id1'>
                <input type="text" name = "name1" id = 'name1'>
                <input type="text" name = "gender1" id = 'gender1'>
                <input id = "file1" type="file" name = "photo" accept="image/*" onchange='document.getElementById("1").src = window.URL.createObjectURL(this.files[0]);'>
                <input id = "file2" type="file" name = "aadhar" accept="image/*" onchange='document.getElementById("2").src = window.URL.createObjectURL(this.files[0]);'>
                <input id = "file3" type="file" name = "license" accept="image/*" onchange='document.getElementById("3").src = window.URL.createObjectURL(this.files[0]);'>
                <input id = "file4" type="file" name = "other" accept="image/*" onchange='document.getElementById("4").src = window.URL.createObjectURL(this.files[0]);'>
                <input type="submit" id = 'updatesubmit'>
            </form>
        </div>

    </body>
</html>

<style>
    .box2
    {
     <?php
      if($global == false)
       {
          echo 'display:none';
       }
       else
       {
          echo 'display:flex';
       }
     ?>
    }
    .popup_upload1
    {
     <?php
      if($error)
       {
          echo 'display: flex';
       }
     ?>
    }
    .content2{
        <?php
            if($error)
            {
                echo "display: flex";
            }
        ?>
    }
</style>
<script>
    
function deletes()
{
    var data = "<?php echo isset($_SESSION['staffname']) ?>";
    if(data != '')
    {
        var val = prompt("Need admin password");
        if(val != '' && val == "<?php if(isset($_SESSION['adminpassword'])){echo $_SESSION['adminpassword'];} ?>")
        {      
            window.location.replace('delete/staffdelete.php');
        }
        else{
            if(val != null)
            {
                alert('Worng password');
                deletes();
            }
        }
    }
    else
    {
        alert('Select the staff');
    }
}

function download()
{ 
    var data = "<?php echo isset($_SESSION['staffname']) ?>";
   if(data == '')
    {
        alert('select the staff');
    }
    else{
         bool = false;
    document.querySelector(".check").style.display = "flex"; 
    document.querySelector(".disablebutton").style.display = "none";
    document.querySelector(".downloadoption").style.display = "flex";     
    }
}

function update()
{ 
    var data = "<?php echo isset($_SESSION['staffname']) ?>";
   if(data == '')
    {
        alert('select the staff');
    }
    else{
    bool = 'update';
    document.getElementById("update").style.display = "flex"; 
    document.querySelector(".update").style.display = "none";
    document.getElementById("gendertext").style.display = 'none';
    document.getElementById("genderradio").style.display = 'block';
    var val = '<?php if(isset($gender)) echo $gender; ?>';
    if(val == 'male')
    {
        document.getElementsByName("gender")[0].checked = true;
    }  
    else{
        document.getElementsByName("gender")[1].checked = true;
    }
    document.getElementById("formsubmit").disabled = true;
    document.getElementById("formreset").disabled = true;

 }
}
function tablevalues(val)
{
    var id = val.childNodes[1].innerHTML;;
    var name = val.childNodes[2].innerHTML;;
    window.location.href = 'staffsearch?id='+id+'&name='+name+'';
}
function updatecancel()
{
    bool = true;
    document.querySelector(".update").style.display = "inline"; 
    document.getElementById("update").style.display = "none";
    var link = "staffsearch?id="+"<?php if(isset($id))echo $id ?>"+"&name="+"<?php if(isset($name)) echo $name ?>";
    window.location.replace(link);
}
function optionclose()
{
    document.querySelector(".options").style.display = "none";
    document.querySelector(".totallist").style.display = "block";
    downloadcancel();
    var data = "<?php echo isset($_SESSION['staffname']) ?>";
    if(data == '')
    {
        window.location.replace('staffsearch');
    }
    else
    {
        window.location.replace("staffsearch?id="+"<?php if(isset($id))echo $id ?>"+"&name="+"<?php if(isset($name)) echo $name ?>");
    }
}
</script>

