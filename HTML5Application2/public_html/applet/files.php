<?php 
    
    session_start();    

    if (!(isset($_SESSION['username'])))
    {
      die('<h1>Direct File Access Prohibited</h1>');
    }
    
    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'staffdocuments';


    $conn = mysqli_connect($servername ,$username , $password , $database);

    if(!empty($_FILES['file']))
    {
    $company = $_POST['company'];
    $documentclass = $_POST['document'];

    $file = addslashes(file_get_contents($_FILES['file']['tmp_name']));
    $filename = basename($_FILES['file']['name'],'.pdf');
    $key =  $_POST['key'];
    $val3 = $_POST['val3'];
    $val4 = $_POST['val4'];
    $val5 = $_POST['val5'];
    $val6 = $_POST['val6'];
    $val7 = $_POST['val7'];
    $val8 = $_POST['val8'];
    $date = date("Y-m-d");

    if($documentclass == 'Invoice')
    {
         $sql = "INSERT INTO productdocuments (company,documentclass, customername , invoicenumber,purchasenumber,orderdate,comments,amount,file,filename,dates,keyvalues) VALUES( '".$company."' ,'".$documentclass."' ,'".$val3."' , '".$val4."' ,'".$val6."', '".$val5."', '".$val7."', '".$val8."','".$file."','".$filename."','".$date."','".$key."');";

    }
    else if($documentclass == 'Shipment')
    {
         $sql = "INSERT INTO productdocuments (company,documentclass,invoicenumber,ordernumber,shipmentdate,orderdate,comments,file,filename,dates,keyvalues) VALUES( '".$company."' ,'".$documentclass."' ,'".$val4."' ,'".$val6."', '".$val3."', '".$val5."', '".$val7."','".$file."','".$filename."','".$date."','".$key."');";

    }
    else
    {
         $sql = "INSERT INTO productdocuments (company,documentclass, customername ,customernumber,podate,ponumber,comments,amount,file,filename,dates,keyvalues) VALUES( '".$company."' ,'".$documentclass."' ,'".$val3."' , '".$val4."' ,'".$val5."', '".$val6."', '".$val7."', '".$val8."','".$file."','".$filename."','".$date."','".$key."');";

    }

           $result = mysqli_query($conn,$sql);
           if($result)
           {
               $global = true;
               $finalmsg = 'success';
           }
           else
           {
              $global = true;
              $finalmsg = 'error';
           }    
    }

?>

<html>
    <head>
        <title>Files</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="stylesheet" type="text/css" href="css/dochomecs.css"> 
        <link rel="stylesheet" type="text/css" href="css/filescss.css">
        <script src = "scripts/script.js"></script>
        <script src = "scripts/script2.js"></script>
        <link rel="shortcut icon" href="images/appletlogo.png">
    </head>
    <body onload = "fileload()">
        
       <div class="wrapper">   
        <div class="sidebar">
            <header>Product Details</header>
            <ul>
                <li><a href="dochome"><i class="fas fa-home"></i>Home</a></li>
                <li><a href="#"><i class="fas fa-file"></i>Files</a></li>
                <li><a href="search"><i class="fas fa-search"></i>Search</a></li>
                <li><a href="#"><i class="fas fa-cog"></i>Settings</a></li>
                <li><a href="main"><i class="fas fa-lock"></i>Log Out</a></li>
            </ul>
          
        </div>
        
        <div class="main_content">
          <form action =  'files' method = 'POST' enctype="multipart/form-data">
            <div class="header">Welcome Files!!!</div>
              <div class="document">
                <label for="company">Company Name</label>
                <select id="company" name="company">
                    <option value="VeeFish Nets">VeeFish Nets</option>
                    <option value="Seba Nets">Seba Nets</option>
                    <option value="Vee Nets">Vee Nets</option>
                </select>
                <br>  <label for="document">Document type</label>
                <select onchange ="documentclass()" id="document" name="document">
                    <option value="Invoice">Invoice</option>
                    <option value="Purchase Orders">Purchase Orders</option>
                    <option value="Shipment">Shipment</option>
                </select>
                <br> <label id = "file" for="file">File</label>
                <input type="file" accept="application/pdf" name="file" placeholder="choose file" required="">
                <br> <label for="key">Key value</label>
                <input type="text" name="key" placeholder="Key value">
                <br> <label id = "name" for="cname">Customer Name</label>
                <input id ="box1" type="text" name="val3" placeholder="customer name">
                <br><label id = "num" for="puchasenum">Purchase Number</label>
                <input id = "box2" type="text" name="val6" placeholder="Purchase number">
                <br> <label id = "number" for="inumber">Invoice Number</label>
                <input type="text" name="val4" id="inumbox" placeholder="Invoice number">
                <br><label id= "date" for="orderdate">Order Date</label>
                <input type="date" name="val5" id="pudate">
                <br><label for="comments">Comments</label>
                <input type="text" name="val7" placeholder="Comments">
                <br><label id = "amt" for="amount">Amount</label>
                <input id ="amtbox" type="text" name="val8" placeholder="Amount">
                <br><input type="submit" name="" value="Submit">
                <br><input type="reset" id = 'reset' name="" value="Reset">
              </div>
            </div>
          </form>

          <div class = "popup_upload1" id = "popupupload1">
            <div class = "content">
                <i class="fas fa-check"></i>
                <h4 style = "font-family:courier,arial,helvetica;margin : 15px;">successfully submitted!</h4>
                <button onclick = 'call2()'>Ok</button>
            </div>
            <div class = "content2">
                <i class="fas fa-times"></i>
                <h4 style = "font-family:courier,arial,helvetica;margin : 15px;">you selected unproper file!</h4>
                <button onclick = 'call2()'>Ok</button>
            </div>
        </div>
           
          

       </div>
           
    </body>
</html>
<script>

</script>

<style>
    .popup_upload1
    {
     <?php
      if($global == false)
       {
          echo 'display: none';
       }
       else
       {
          echo 'display:flex';
       }
     ?>
    }

    .content
    {
      <?php
      if($finalmsg == "success")
         {
            echo 'display:flex';
         }
       ?>
    }

    .content2
    {
      <?php
        if($finalmsg == "error")
        {
           echo 'display:flex';
        }
       ?>
    }
</style>

<script>

</script>
