<?php 
   
    include("connection.php");

    $date = date("Y-m-d");
    $sql = "SELECT * FROM production_boxes WHERE scanneddate = '".$date."';";
    
    $bag_result = mysqli_query($conn,$sql);

    $check = mysqli_num_rows($bag_result);

?>

<html lang="en">
<head>
  <title>YARN STOCK REPORT</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/report.css">
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="shortcut icon" href="images/applogo.jpg">
   <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
   <script src="scripts/production.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Home">Victorial Filament And Net</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Home">Home</a></li>
      <li><a href="Purchase">Purchase</a></li>
      <li><a href="Production">Production</a></li>
      <li class="dropdown" class="active">
          <a style = "background: white;color: black;border-radius: 5px;" href="#">Report</a>
          <div class="dropdown-content">
            <a href="Yarn_Stock_Report">Yarn Stock</a>
            <a href="Yarn_Production_Report">Yarn Production</a>
            <a href="Twine_Stock_Report">Twine Stock</a>
            <a href="Twine_Dispatch_Report">Twine Dispatch</a>
          </div>
      </li>
    </ul>
    
     <ul class="nav navbar-nav" style="float: right">
        <li><?php if(!(isset($_COOKIE['id']))){ echo "<a href='Login'>Login</a>"; }else{ echo "<a href='logout.php'>Logout</a>";} ?></li>
     </ul>

   </div>
</nav>

<?php
    if (!(isset($_COOKIE['id'])))
    {
      die('<h3 style = "margin : 40px;">Direct File Access Prohibited You Want To Login First</h3>');
    }
?>

<div class = "container-fluid" style = "margin : 0px 40px 0px 40px;">
    <h2><u>YARN PRODUCTION</u></h2>
    <div class = "container-fluid searchinputs" style = "margin-top: 40px;">
      <input onfocus="" onblur="" onchange="search()" id="date" type="date" placeholder="Date" value = "<?php echo $date; ?>">

      <select id = "size" onchange = "search()">
          <option>All Sizes</option>
          <option value="210D">210 D</option>
          <option value="420D">420 D</option>
          <option value="630D">630 D</option>
          <option value="840D">840 D</option>
          <option value="1050D">1050 D</option>
          <option value="1260D">1260 D</option>
      </select>
    </div>
    <!-- Lists --->
    <div class = "container-fluid">
        <h3>Yarn Bags Used</h3>
        <div class = "groups" id = "group1">
         <!-- <div class = "lists">
              <div class = "l1">
                <label>Purchase No</label>
                <label>28982</label><br>
                <label>Purchase Date</label>
                <label>2/2/2000</label>
              </div>
              <div class = "l2">
                <label>Amal</label><label>210 D</label>
                <i onclick="get_full_list()" class="fas fa-external-link-alt"></i>
              </div>  
              <div class = "l3">
                <table>
                  <tbody><tr><td><h5>Total<br>Bags</h5></td><td><label>28982</label></td><td><h5>Total<br>Netwt</h5></td><td><label>2/2/200077</label></td><td><h5>Total<br>Grosswt</h5></td><td><label>2/2/20005667</label></td></tr></tbody>
                </table>
              </div>        
          </div> -->
          <?php
            if($check>0)
            {
              while($row = mysqli_fetch_assoc($bag_result))
              {
                   $sql = "SELECT * FROM production WHERE uniqueid = '".$row["uniqueid"]."';";
                   $result = mysqli_query($conn,$sql);
                   $check = mysqli_num_rows($result);
                   if($check > 0)
                   {
                     $rows = mysqli_fetch_assoc($result);
                     echo '<div class = "lists">
                          <div class = "l1">
                            <label>Purchase No</label>
                            <label>'.$rows["purchasenumber"].'</label><br>
                            <label>Purchase Date</label>
                            <label>'.$rows["purchasedate"].'</label>
                          </div>
                          <div class = "l2">
                            <label>'.$rows["customer"].'</label><label>'.$row["size"].'</label>
                            <i onclick=get_full_list('.$row["uniqueid"].','.$rows["purchasenumber"].',"'.$rows['purchasedate'].'","'.$rows['size'].'","'.$rows["customer"].'",'.$rows["bags"].','.$rows["totalnetweight"].','.$rows["totalgrossweight"].') class="fas fa-external-link-alt"><span class = "tipname">View full list</span></i>
                          </div>  
                          <div class = "l3">
                            <table>
                              <tbody><tr><td><h5>Bag No</h5></td><td><label>'.$row["boxnumber"].'</label></td><td><h5>Bag Date</h5></td><td><label>'.$row["box_date"].'</label></td><td><h5>Netwt</h5></td><td><label>'.$row["netweight"].'</label></td><td><h5>Grosswt</h5></td><td><label>'.$row["grossweight"].'</label></td></tr></tbody>
                            </table>
                          </div>        
                         </div>';
                    }
              } 
            }
              
          ?>
        </div>
    </div>

   
</div>

<!-- view full -->
<div class = "viewfull" id = "viewfull">
  <h1>VIEW LIST</h1>
  <i class="fas fa-times" onclick = "document.getElementById('viewfull').style.display = 'none'"></i>
  <div class = "container">
      <div class = "boxes">
                <div class = "bagsinhand">
                    <i class="fas fa-cubes"></i>
                    <h3 id = 'remain'></h3>
                    <p>BAGS IN HAND</p>
                </div>
                <div class = "bagsinhand">
                    <i class="fas fa-weight"></i>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">N</p>
                      <h4 id='remainnetwt'></h4>
                    </div>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">G</p>
                      <h4 style="" id='remaingrosswt'></h4>
                    </div>
                    <p>WEIGHTS IN HAND</p>
                </div>
                <div class = "bagssold">
                    <i class="fas fa-cubes"></i>
                    <h3 id = "used"></h3>
                    <p>BAGS USED</p>
                </div>
                <div class = "bagssold">
                    <i class="fas fa-weight"></i>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">N</p>
                      <h4 id='usednetwt'></h4>
                    </div>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">G</p>
                      <h4 id="usedgrosswt"></h4>
                    </div>
                    <p>WEIGHTS USED</p>
                </div>
            </div>

            <div class="listbox">
                <div class = "listdetails2">
                  <p>List ID</p>
                  <h5 id = 'viewuid'></h5>
                </div>
                <div class = "listdetails2">
                  <p>Purchase Number</p>
                  <h5 id = "viewpno"></h5>
                </div>
                <div class = "listdetails2">
                  <p>Purchase Customer</p>
                  <h5 id = "viewpcustomer"></h5>
                </div>
                <div class = "listdetails2">
                  <p>Purchase Date</p>
                  <h5 id = "viewdate"></h5>
                </div>
                <div class = "listdetails2">
                  <p>Size</p>
                  <h5 id = "viewsize"></h5>
                </div>
                <div class = "listdetails2">
                  <p>Total Bags</p>
                  <h5 id = "viewtbags" style="color: #2cc6de;"></h5>
                </div>
                <div class = "listdetails2">
                  <p>Total Netweight</p>
                  <h5 id = "viewtnetwt" style="color: #2cc6de;"></h5>
                </div>
                <div class = "listdetails2">
                  <p>Total Grossweight</p>
                  <h5 id = "viewtgrosswt" style="color: #2cc6de;"></h5>
                </div>
            </div>

            <table class="table table-bordered table-striped">
                  <thead>
                      <tr><th>Box Number</th><th>Size</th><th>Box Date</th><th>Net Weight</th><th>Gross Weight</th><th>Scanned Date</th><th>Qr Code</th></tr>
                  </thead>

                  <tbody id="viewtable">
                  </tbody>
            </table>
  </div>
</div>

</body>
</html>

<style>

.viewfull{
  display: none;
  position: fixed; 
  z-index: 1; 
  left: 0;
  top: 0;
  width: 100%; 
  height: 100%; 
  overflow: auto; 
  background-color: rgba(0,0,0,0.6); 
}
.viewfull h1{
  position: fixed;
  top : 30px;
  left : 90px;
  color: white;
}
/* close icon */
.viewfull .fa-times{
  position: fixed;
  top : 40px;
  right : 100px;
  color: white;
  font-size: 40px;
  cursor: pointer;
}

.viewfull .container{
  margin-top : 130px;
  margin-bottom: 100px;
  background: white;
  border-radius: 5px;
}
</style>

<script>
function search()
{
    var link = "phpback/syarn_production_report.php?db=production_boxes&bdb=production";
    if(document.getElementById("date").value != "")
    {
        link += "&date="+document.getElementById("date").value;
    }
    if(document.getElementById("size").value != "All Sizes")
    {
        link += "&size="+document.getElementById("size").value;
    }
    
    var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() 
      {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('$');
            document.getElementById("group1").innerHTML = parts[0];
        }
      };
      xhttp.open("GET",link, true);
      xhttp.send();   
}

function get_full_list(id,pno,pdate,size,customer,bags,totalnetwt,totalgrosswt)
{
    document.getElementById("viewfull").style.display = "block";
    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() 
        {
          if(this.readyState == 4 && this.status == 200)
          {
              var parts = this.responseText.split('$');
              document.getElementById("viewuid").innerHTML = id;
              document.getElementById("viewpno").innerHTML = pno;
              document.getElementById("viewpcustomer").innerHTML = customer;
              document.getElementById("viewsize").innerHTML = size;
              document.getElementById("viewdate").innerHTML = pdate;
              document.getElementById("viewtbags").innerHTML = bags;
              document.getElementById("viewtnetwt").innerHTML = totalnetwt;
              document.getElementById("viewtgrosswt").innerHTML = totalgrosswt; 

              document.getElementById("viewtable").innerHTML = parts[0];
              document.getElementById("remain").innerHTML = bags-parts[1];
              document.getElementById("used").innerHTML = parts[1];
              document.getElementById("remainnetwt").innerHTML = totalnetwt-parts[2];
              document.getElementById("remaingrosswt").innerHTML = totalgrosswt-parts[3];
              document.getElementById("usednetwt").innerHTML = parts[2];
              document.getElementById("usedgrosswt").innerHTML = parts[3];
          }
        };
        xhttp.open("GET", "phpback/searchview.php?id="+id+"&type=production_boxes", true);
        xhttp.send(); 
}


</script>