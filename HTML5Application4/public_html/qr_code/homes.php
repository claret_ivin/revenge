<?php 
  
  $pcheck = 0; $scheck = 0;$pbags=$sbags=$pnetwt=$snetwt=$pgrosswt=$sgrosswt=$tbags=$tnetwt=$tgrosswt=0;
  include("connection.php");
  
  $sql = "SELECT * FROM production WHERE size = '3ply' ORDER BY id DESC LIMIT 1;";

  $details = mysqli_query($conn,$sql);

  if($details) $check = mysqli_num_rows($details);

  if($check>0)
  {
      $rows = mysqli_fetch_assoc($details);
      
      $sql = "SELECT * FROM production_boxes WHERE uniqueid = '".$rows['uniqueid']."';";

      $pdetails = mysqli_query($conn,$sql);
      if($pdetails) $pcheck = mysqli_num_rows($pdetails);
  }

 /* $sql = "SELECT * FROM stock WHERE size = '3ply' ORDER BY id DESC LIMIT 1;";

  $details = mysqli_query($conn,$sql);

  if($details) $check = mysqli_num_rows($details);

  if($check>0)
  {
      $srows = mysqli_fetch_assoc($details);
      
      $sql = "SELECT * FROM stock_boxes WHERE uniqueid = '".$srows['uniqueid']."';";

      $sdetails = mysqli_query($conn,$sql);
      if($sdetails) $scheck = mysqli_num_rows($sdetails);
  } */

  $sql = "SELECT * FROM stock_boxes WHERE size = '3ply' AND scanneddate = '';";

  $sdetails = mysqli_query($conn,$sql);
  if($sdetails) $scheck = mysqli_num_rows($sdetails);

?> 

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/home.css">
  <link rel="shortcut icon" href="images/applogo.jpg">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="scripts/home.js"></script>
  
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="Home">Victorial Filament And Net</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a style = "background: white;color: black;border-radius: 5px;" href="#">Home</a></li>
      <li><a href="Purchase">Purchase</a></li>
      <li><a href="Production">Production</a></li>
      <li class="dropdown" class="active">
          <a href="#">Report</a>
          <div class="dropdown-content">
            <a href="Yarn_Stock_Report">Yarn Stock</a>
            <a href="Yarn_Production_Report">Yarn Production</a>
            <a href="Twine_Stock_Report">Twine Stock</a>
            <a href="Twine_Dispatch_Report">Twine Dispatch</a>
          </div>
      </li>
     </ul>
      <ul class="nav navbar-nav" style="float: right">
        <li><?php if(!(isset($_COOKIE['id']))){ echo "<a href='Login'>Login</a>"; }else{ echo "<a href='logout.php'>Logout</a>";} ?></li>
     </ul>
  </div>
</nav>

<?php
    if (!(isset($_COOKIE['id'])))
    {
      die('<h3 style = "margin : 40px;">Direct File Access Prohibited You Want To Login First</h3>');
    }
?>

<div class = "container-fluid">
  <!-- current production --->
  <div  class="col-12 col-md-6">
      <h3 style = "  margin-bottom: 30px;"><u>Current Purchase</u></h3>
      <select style="margin-right : 20px;" id = "psize" onchange = "prosearchsize()">
          <option value="3ply">3Ply</option>
          <option value="4ply">4Ply</option>
          <option value="6ply">6Ply</option>
          <option value="8ply">8Ply</option>
          <option value="9ply">9Ply</option>
          <option value="16ply">16Ply</option>
      </select>
      <input style="margin-right : 20px;" id = "pdate" type = "text" onfocus="datechange(this)" onblur="textchange(this)" onchange="search_plist_with_date()" placeholder="Search List With Date" />
      <input style="width : 100px;" onclick = "clear_dates()" value = "Clear Dates" type = "button" class="btn-danger"/>
    
      <div class = "result">
          <table>
                <thead><tr><th></th><th>Bags</th><th>Netwt</th><th>Grosswt</th></tr></thead>
                <tbody>
                  <tr class = "remains"><td><h4>Remain</h4></td>
                      <td><label id = 'bremain'></label></td>
                      <td><label id = 'nremain'></label></td>
                      <td><label id = 'gremain'></label></td>
                  </tr>
                  <tr class = "useds"><td><h4>Used</h4></td>
                      <td><label id = 'bused'></label></td>
                      <td><label id = 'nused'></label></td>
                      <td><label id = 'gused'></label></td>
                  </tr>
                </tbody>
          </table>
      </div>
     
      <div class="form-group row">
          <label class = "col-sm-3 col-form-label">List ID</label>
          <label id = "pid" style = "color: silver"><?php if(isset($rows['uniqueid'])){ echo $rows['uniqueid'];}?></label>
      </div>
      <div class="form-group row">
          <label class = "col-sm-3 col-form-label">Purchase Number</label>
          <label id = "pnum" style = "color: silver"><?php if(isset($rows['purchasenumber'])){ echo $rows['purchasenumber'];}?></label>
      </div>
      <div class="form-group row">
          <label class = "col-sm-3 col-form-label">Purchase Date</label>
          <label id = "purchasedate" style = "color: silver"><?php if(isset($rows['purchasedate'])){ echo $rows['purchasedate'];}?></label>
      </div>
      <div class="form-group row">
          <label class = "col-sm-3 col-form-label">Purchase Customer</label>
          <label id = "pcustomer" style = "color: silver"><?php if(isset($rows['customer'])){ echo $rows['customer'];}?></label>
      </div>

      <table class="table table-bordered" style = "width: 600px;">
          <thead><tr><th>Box no</th><th>Size</th><th>Date</th><th>Net wt</th><th>Gross wt</th><th>Scanned Date</th><th>Qrcode</th></tr></thead>
          <tbody  id = "ptable">
            <?php

                if($pcheck > 0)
                {
                    while($row = mysqli_fetch_assoc($pdetails))
                    {
                        echo "<tr onclick = 'tablevalues1(this)'><td>".$row['boxnumber']."</td><td>" .$row['size']."</td><td>".$row['box_date']."</td><td>" .$row['netweight']."</td><td>".$row['grossweight']."</td><td>" .$row['scanneddate']."</td><td><img onclick='showqrcode(this)' width = 35 onclick='showqrcode(this)' src = "."data:image/jpg:chasrset-utf8;base64,".base64_encode($row['qrcode'])." /></td></tr>";

                        if($row['scanneddate'] != '')
                        {
                            $pbags++;
                            $pnetwt += $row['netweight'];
                            $pgrosswt += $row['grossweight'];
                        }
                    } 
                }
            ?>
          </tbody>
      </table>

  </div>

    <!-- current stock --->
  <div  class="col-12 col-md-6">
      <h3  style = "  margin-bottom: 30px;"><u>Current Production</u></h3>
      <select style="margin-right : 20px;" id = "ssize" onchange = "stocksearchsize()">
          <option value="3ply">3Ply</option>
          <option value="4ply">4Ply</option>
          <option value="6ply">6Ply</option>
          <option value="8ply">8Ply</option>
          <option value="9ply">9Ply</option>
          <option value="16ply">16Ply</option>
      </select>
      <input style="margin-right : 20px;" id = "sdate" type = "text" onfocus="datechange(this)" onblur="textchange(this)" onchange = "search_slist_with_date()" placeholder="Search List With Date" />
      <input style="width : 100px;" onclick = "clear_dates_2()" value = "Clear Dates" type = "button" class="btn-danger"/>
     
      <div class = "result">
          <table>
                <thead><tr><th></th><th>Bags</th><th>Netwt</th><th>Grosswt</th></tr></thead>
                <tbody>
                  <tr class = "remains"><td><h4>Remain</h4></td>
                      <td><label id = 'sbremain'></label></td>
                      <td><label id = 'snremain'></label></td>
                      <td><label id = 'sgremain'></label></td>
                  </tr>
                  <tr class = "useds"><td><h4>Sold</h4></td>
                      <td><label id = 'sbused'></label></td>
                      <td><label id = 'snused'></label></td>
                      <td><label id = 'sgused'></label></td>
                  </tr>
                </tbody>
          </table>
      </div>

      <div class="form-group row">
          <label class = "col-sm-3 col-form-label">List ID</label>
          <label id = "sid" style = "color: silver"><?php if(isset($srows['uniqueid'])){ echo $srows['uniqueid'];}?></label>
      </div>
      <div class="form-group row">
          <label class = "col-sm-3 col-form-label">Production Number</label>
          <label id = "snum" style = "color: silver"><?php if(isset($srows['stocknumber'])){ echo $srows['stocknumber'];}?></label>
      </div>
      <div class="form-group row">
          <label class = "col-sm-3 col-form-label">Production Date</label>
          <label id = "stockdate" style = "color: silver"><?php if(isset($srows['stockdate'])){ echo $srows['stockdate'];}?></label>
      </div>
      
      <table class="table table-bordered" style = "width: 600px;">
        <thead><tr><th>Box no</th><th>Size</th><th>Date</th><th>Net wt</th><th>Gross wt</th><th>Scanned Date</th><th>Customer</th><th>Qrcode</th></tr></thead>
        <tbody  id = "stable">
          <?php

              if($scheck > 0)
              {
                  while($row = mysqli_fetch_assoc($sdetails))
                  {
                      echo "<tr onclick = 'tablevalues2(this)'><td>".$row['boxnumber']."</td><td>" .$row['size']."</td><td>".$row['box_date']."</td><td>" .$row['netweight']."</td><td>".$row['grossweight']."</td><td>" .$row['scanneddate']."</td><td>" .$row['customer']."</td><td><img onclick='showqrcode2(this)' width = 35 onclick='showqrcode(this)' src = "."data:image/jpg:chasrset-utf8;base64,".base64_encode($row['qrcode'])." /></td></tr>";

                       $tbags++;$tnetwt += $row['netweight'];$tgrosswt += $row['grossweight'];

                        if($row['scanneddate'] != '')
                        {
                            $sbags++;
                            $snetwt += $row['netweight'];
                            $sgrosswt += $row['grossweight'];
                        }
                  } 
              }
          ?>
        </tbody>
      </table>
  </div>

</div>

<!-- Modal qrcode show -->
                    <div class="modal fade" id="myModal1" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">PURCHASE QRCODE</h4>
                          </div>
                          <div class="modal-body">
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Unique ID</label>
                                   <input type="text" placeholder="uniqueid" id='codeid' readonly="" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Box number</label>
                                   <input type="text" placeholder="Box Number" id='codeno' readonly="" /><br>
                                </div>
                                <div id = "modalimgdiv"><img style="margin-left: 150px;" id = "modalimg" /></div>
                          </div>
                          <div class="modal-footer">
                            <button type="button"class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="button"class="btn btn-primary" onclick = "imagecheck('modalimgdiv')">Print</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!----- modal qrcode show end -->
<!-- Modal qrcode show -->
                    <div class="modal fade" id="myModal2" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">PRODUCTION QRCODE</h4>
                          </div>
                          <div class="modal-body">
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Unique ID</label>
                                   <input type="text" placeholder="uniqueid" id='scodeid' readonly="" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Box number</label>
                                   <input type="text" placeholder="Box Number" id='scodeno' readonly="" /><br>
                                </div>
                                <div id = "modalimgdiv2"><img style="margin-left: 150px;" id = "modalimg2" /></div>
                          </div>
                          <div class="modal-footer">
                            <button type="button"class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="button"class="btn btn-primary" onclick = "imagecheck('modalimgdiv2')">Print</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!----- modal qrcode show end -->

</body>
</html>

<style>
.dropdown-content a {
  color: #f5f5f5;
  text-decoration: none;
}
</style>


<Script>

  document.getElementById('bremain').innerHTML = <?php echo $rows['bags']-$pbags; ?>;
  document.getElementById('nremain').innerHTML = <?php echo $rows['totalnetweight']-$pnetwt; ?>;
  document.getElementById('gremain').innerHTML = <?php echo $rows['totalgrossweight']-$pgrosswt; ?>;
  document.getElementById('bused').innerHTML = <?php echo $pbags; ?>;
  document.getElementById('nused').innerHTML = <?php echo $pnetwt; ?>;
  document.getElementById('gused').innerHTML = <?php echo $pgrosswt; ?>;
  document.getElementById('sbremain').innerHTML = <?php echo $tbags-$sbags; ?>;
  document.getElementById('snremain').innerHTML = <?php echo $tnetwt-$snetwt; ?>;
  document.getElementById('sgremain').innerHTML = <?php echo $tgrosswt-$sgrosswt; ?>;
  document.getElementById('sbused').innerHTML = <?php echo $sbags; ?>;
  document.getElementById('snused').innerHTML = <?php echo $snetwt; ?>;
  document.getElementById('sgused').innerHTML = <?php echo $sgrosswt; ?>;


function prosearchsize()
{
  var str = document.getElementById("psize").value;
  var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() 
      {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('$');
            if(parts[1] == 'false')
            {
                alert(str+" size not in database...");
                document.getElementById("pid").innerHTML = '';
                document.getElementById("pnum").innerHTML = '';
                document.getElementById("purchasedate").innerHTML = '';
                document.getElementById("pcustomer").innerHTML = '';
                document.getElementById("ptable").innerHTML = '';
                document.getElementById('bremain').innerHTML = '';
                document.getElementById('nremain').innerHTML = '';
                document.getElementById('gremain').innerHTML = '';
                document.getElementById('bused').innerHTML = '';
                document.getElementById('nused').innerHTML = '';
                document.getElementById('gused').innerHTML = '';
            }
            else
            {
                document.getElementById("pid").innerHTML = parts[0];
                document.getElementById("pnum").innerHTML = parts[1];
                document.getElementById("purchasedate").innerHTML = parts[2];
                document.getElementById("pcustomer").innerHTML = parts[3];
                
                document.getElementById('bremain').innerHTML = parts[4]-parts[8];
                document.getElementById('nremain').innerHTML = parts[5]-parts[9];
                document.getElementById('gremain').innerHTML = parts[6]-parts[10];
                document.getElementById('bused').innerHTML = parts[8];
                document.getElementById('nused').innerHTML = parts[9];
                document.getElementById('gused').innerHTML = parts[10];
                document.getElementById("ptable").innerHTML = parts[7];
            }
        }
      };
      xhttp.open("GET", "phpback/current.php?db=production_boxes&bdb=production&size="+str, true);
      xhttp.send();  
}

function stocksearchsize()
{
  var str = document.getElementById("ssize").value;
  var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() 
      {
          if(this.readyState == 4 && this.status == 200)
          {
            var parts = this.responseText.split('$');
            if(parts[1] == 'false')
            {
                alert(str+" size not in database...");
               // document.getElementById("sid").innerHTML = '';
               // document.getElementById("snum").innerHTML = '';
               // document.getElementById("sdate").innerHTML = '';
                document.getElementById("stable").innerHTML = '';
                document.getElementById('sbremain').innerHTML = '';
                document.getElementById('snremain').innerHTML = '';
                document.getElementById('sgremain').innerHTML = '';
                document.getElementById('sbused').innerHTML = '';
                document.getElementById('snused').innerHTML = '';
                document.getElementById('sgused').innerHTML = '';
            }
            else
            {
               // document.getElementById("sid").innerHTML = parts[0];
               // document.getElementById("snum").innerHTML = parts[1];
               // document.getElementById("sdate").innerHTML = parts[2];
                document.getElementById("stable").innerHTML = parts[0];

              /*  document.getElementById('sbremain').innerHTML = parts[3]-parts[7];
                document.getElementById('snremain').innerHTML = parts[4]-parts[8];
                document.getElementById('sgremain').innerHTML = parts[5]-parts[9];
                document.getElementById('sbused').innerHTML = parts[7];
                document.getElementById('snused').innerHTML = parts[8];
                document.getElementById('sgused').innerHTML = parts[9];  */

                document.getElementById('sbremain').innerHTML = parts[1]-parts[4];
                document.getElementById('snremain').innerHTML = parts[2]-parts[5];
                document.getElementById('sgremain').innerHTML = parts[3]-parts[6];
                document.getElementById('sbused').innerHTML = parts[4];
                document.getElementById('snused').innerHTML = parts[5];
                document.getElementById('sgused').innerHTML = parts[6];
            }
          }
      };
      xhttp.open("GET", "phpback/current.php?db=stock_boxes&bdb=stock&size="+str, true);
      xhttp.send();  
}

function tablevalues1(val)
{
      document.getElementById('codeid').value = document.getElementById('pid').innerHTML;
      document.getElementById('codeno').value = val.childNodes[0].innerHTML;
}

function tablevalues2(val)
{
      document.getElementById('scodeid').value = document.getElementById('sid').innerHTML;
      document.getElementById('scodeno').value = val.childNodes[0].innerHTML;
}

// to show the particular purchase qrcode
function showqrcode(val)
{
    document.getElementById('modalimg').src = val.src;
    $('#myModal1').modal('show'); 
}

// to show the particular production qrcode
function showqrcode2(val)
{
    document.getElementById('modalimg2').src = val.src;
    $('#myModal2').modal('show'); 
}

function datechange(val)
{
     val.type = 'date';
     //
}

function textchange(val)
{
  val.type = 'text';
  //pass
}

function search_plist_with_date()
{
    var date = document.getElementById("pdate").value;
    var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() 
      {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('$');
            if(parts[1] == 'false')
            {
                alert(date +" date not in database...");
                document.getElementById("pid").innerHTML = '';
                document.getElementById("pnum").innerHTML = '';
                document.getElementById("purchasedate").innerHTML = '';
                document.getElementById("pcustomer").innerHTML = '';
                document.getElementById("ptable").innerHTML = '';
                document.getElementById('bremain').innerHTML = '';
                document.getElementById('nremain').innerHTML = '';
                document.getElementById('gremain').innerHTML = '';
                document.getElementById('bused').innerHTML = '';
                document.getElementById('nused').innerHTML = '';
                document.getElementById('gused').innerHTML = '';
            }
            else
            {
                document.getElementById("pid").innerHTML = parts[0];
                document.getElementById("pnum").innerHTML = parts[1];
                document.getElementById("purchasedate").innerHTML = parts[2];
                document.getElementById("pcustomer").innerHTML = parts[3];
                
                document.getElementById('bremain').innerHTML = parts[4]-parts[8];
                document.getElementById('nremain').innerHTML = parts[5]-parts[9];
                document.getElementById('gremain').innerHTML = parts[6]-parts[10];
                document.getElementById('bused').innerHTML = parts[8];
                document.getElementById('nused').innerHTML = parts[9];
                document.getElementById('gused').innerHTML = parts[10];
                document.getElementById("ptable").innerHTML = parts[7];
            }
        }
      };
      xhttp.open("GET", "phpback/search_list_with_date.php?db=production_boxes&bdb=production&date="+date, true);
      xhttp.send();   
}

function search_slist_with_date()
{
    var date = document.getElementById("sdate").value;
    var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() 
      {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('$');
            if(parts[1] == 'false')
            {
                alert(date +" date not in database...");
                document.getElementById("sid").innerHTML = '';
                document.getElementById("snum").innerHTML = '';
                document.getElementById("stockdate").innerHTML = '';
               // document.getElementById("scustomer").innerHTML = '';
                document.getElementById("stable").innerHTML = '';
                document.getElementById('sbremain').innerHTML = '';
                document.getElementById('snremain').innerHTML = '';
                document.getElementById('sgremain').innerHTML = '';
                document.getElementById('sbused').innerHTML = '';
                document.getElementById('snused').innerHTML = '';
                document.getElementById('sgused').innerHTML = '';
            }
            else
            {
                document.getElementById("sid").innerHTML = parts[0];
                document.getElementById("snum").innerHTML = parts[1];
                document.getElementById("stockdate").innerHTML = parts[2];
               // document.getElementById("scustomer").innerHTML = parts[3];
                
                document.getElementById('sbremain').innerHTML = parts[3]-parts[7];
                document.getElementById('snremain').innerHTML = parts[4]-parts[8];
                document.getElementById('sgremain').innerHTML = parts[5]-parts[9];
                document.getElementById('sbused').innerHTML = parts[7];
                document.getElementById('snused').innerHTML = parts[8];
                document.getElementById('sgused').innerHTML = parts[9];
                document.getElementById("stable").innerHTML = parts[6];
            }
        }
      };
      xhttp.open("GET", "phpback/search_list_with_date.php?db=stock_boxes&bdb=stock&date="+date, true);
      xhttp.send();   
}

// to clear the dates.
function clear_dates()
{
    document.getElementById('pdate').value = '';
    document.getElementById("psize").value = '3ply';
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('$');
            if(parts[1] == 'false')
            {
                alert('Not loaded..');
                document.getElementById("pid").innerHTML = '';
                document.getElementById("pnum").innerHTML = '';
                document.getElementById("purchasedate").innerHTML = '';
                document.getElementById("pcustomer").innerHTML = '';
                document.getElementById("ptable").innerHTML = '';
                document.getElementById('bremain').innerHTML = '';
                document.getElementById('nremain').innerHTML = '';
                document.getElementById('gremain').innerHTML = '';
                document.getElementById('bused').innerHTML = '';
                document.getElementById('nused').innerHTML = '';
                document.getElementById('gused').innerHTML = '';
            }
            else
            {
                document.getElementById("pid").innerHTML = parts[0];
                document.getElementById("pnum").innerHTML = parts[1];
                document.getElementById("purchasedate").innerHTML = parts[2];
                document.getElementById("pcustomer").innerHTML = parts[3];
                
                document.getElementById('bremain').innerHTML = parts[4]-parts[8];
                document.getElementById('nremain').innerHTML = parts[5]-parts[9];
                document.getElementById('gremain').innerHTML = parts[6]-parts[10];
                document.getElementById('bused').innerHTML = parts[8];
                document.getElementById('nused').innerHTML = parts[9];
                document.getElementById('gused').innerHTML = parts[10];
                document.getElementById("ptable").innerHTML = parts[7];
            }
            
        }
    };
    xhttp.open("GET","phpback/search_list_with_date.php?db=production_boxes&bdb=production&date=null", true);
    xhttp.send();
}

// to clear the dates.
function clear_dates_2()
{
    document.getElementById('sdate').value = '';
    document.getElementById("ssize").value = '3ply';
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('$');
            if(parts[1] == 'false')
            {
                alert("Not loaded..");
                document.getElementById("sid").innerHTML = '';
                document.getElementById("snum").innerHTML = '';
                document.getElementById("sdate").innerHTML = '';
                document.getElementById("stable").innerHTML = '';
                document.getElementById('sbremain').innerHTML = '';
                document.getElementById('snremain').innerHTML = '';
                document.getElementById('sgremain').innerHTML = '';
                document.getElementById('sbused').innerHTML = '';
                document.getElementById('snused').innerHTML = '';
                document.getElementById('sgused').innerHTML = '';
            }
            else
            {
                document.getElementById("sid").innerHTML = '';
                document.getElementById("snum").innerHTML = '';
                document.getElementById("stockdate").innerHTML = '';
                document.getElementById("stable").innerHTML = parts[0];

              /*document.getElementById('sbremain').innerHTML = parts[3]-parts[7];
                document.getElementById('snremain').innerHTML = parts[4]-parts[8];
                document.getElementById('sgremain').innerHTML = parts[5]-parts[9];
                document.getElementById('sbused').innerHTML = parts[7];
                document.getElementById('snused').innerHTML = parts[8];
                document.getElementById('sgused').innerHTML = parts[9];  */

                document.getElementById('sbremain').innerHTML = parts[1]-parts[4];
                document.getElementById('snremain').innerHTML = parts[2]-parts[5];
                document.getElementById('sgremain').innerHTML = parts[3]-parts[6];
                document.getElementById('sbused').innerHTML = parts[4];
                document.getElementById('snused').innerHTML = parts[5];
                document.getElementById('sgused').innerHTML = parts[6];
            }
            
        }
    };
    xhttp.open("GET","phpback/search_list_with_date.php?db=stock_boxes&bdb=stock&date=null", true);
    xhttp.send();
}

function imagecheck(vall)
{
        var contents = document.getElementById(vall).innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame";
        frame1.style.position = "absolute";
        frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html><head><title>Qrcode</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
        window.frames["frame"].focus();
        window.frames["frame"].print();
        document.body.removeChild(frame1);
        }, 500);
    
} 

</Script>
