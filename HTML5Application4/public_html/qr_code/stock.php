<?php 
   
    
    include("connection.php");

    $sql = "SELECT * FROM stock ORDER BY id DESC;";

    $result1 = mysqli_query($conn,$sql);

    $check = mysqli_num_rows($result1);

    $sql = "SELECT * FROM stock_boxes WHERE scanneddate = '' ORDER BY id DESC;";

    $bagsresult = mysqli_query($conn,$sql);

    $bagscheck = mysqli_num_rows($bagsresult);

    $sql = "SELECT * FROM customers;";

    $pcustomer = mysqli_query($conn,$sql);
    
    $pc_check = mysqli_num_rows($pcustomer);
    $customers = array();
    if($pc_check > 0)
    {
        while($rows = mysqli_fetch_assoc($pcustomer))
        {
            $customers[] = $rows['customername'];
        } 
    } 
  
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
      
          $qrcode ='http://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=111012001,'.$uniqueid.','.$company.',stock,'.$size.',';
    }
    
?>

<html lang="en">
<head>
  <title>PRODUCTION</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <link rel="shortcut icon" href="images/applogo.jpg">
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <script src="scripts/stock.js"></script>

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Home">Victorial Filament And Net</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Home">Home</a></li>
      <li><a href="Purchase">Purchase</a></li>
      <li class="active"><a style = "background: white;color: black;border-radius: 5px;" href="#">Production</a></li>
      <li class="dropdown" class="active">
          <a href="#">Report</a>
          <div class="dropdown-content">
            <a href="Yarn_Stock_Report">Yarn Stock</a>
            <a href="Yarn_Production_Report">Yarn Production</a>
            <a href="Twine_Stock_Report">Twine Stock</a>
            <a href="Twine_Dispatch_Report">Twine Dispatch</a>
          </div>
      </li>
    </ul>
      <ul class="nav navbar-nav" style="float: right">
          <li><?php if(!(isset($_COOKIE['id']))){ echo "<a href='Login'>Login</a>"; }else{ echo "<a href='logout.php'>Logout</a>";} ?></li>
     </ul>
   </div>
</nav>

<?php
    if (!(isset($_COOKIE['id'])))
    {
      die('<h3 style = "margin : 40px;">Direct File Access Prohibited You Want To Login First</h3>');
    }
?>

<div class="container">
    <h2 style = "margin-bottom: 20px;">PRODUCTION</h2>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#create">Create List</a></li>
        <li><a data-toggle="tab" href="#viewbags">View Bags</a></li>
        <li><a data-toggle="tab" href="#search">Search List</a></li>
        <li><a data-toggle="tab" id = "viewid" href="#view">View List</a></li>
    </ul>

    <div class="tab-content">
        <div id="create" class="tab-pane fade in active">
           <form>
               <select onchange='customer("lcustomer")' id = 'lcustomer' class ='unid'>
                      <option>Customer</option>
                      <option>New</option>
                      <?php
                          $i =0;
                         
                          while($i < count($customers))
                          {
                             echo "<option>".$customers[$i]."</option>";
                             $i++;
                          }     
                      ?>
               </select>
               <input id='date' class ='unid' type="date" placeholder="Date">
               <input id='pno' class ='unid' type="text" placeholder="Production Number" autocomplete="off">
               <select class ='listsize' id='lsize'>
                      <option value="3ply">3 Play</option>
                      <option value="4ply">4 Ply</option>
                      <option value="6ply">6 Ply</option>
                      <option value="8ply">8 Ply</option>
                      <option value="9ply">9 Ply</option>
                      <option value="16ply">16 Ply</option>
               </select>
               <input id = 'unid' class ='unid' type="text" placeholder="List ID" readonly="">
               <button type="button" onclick='createlist()' class="btn btn-primary idbtn">Create New List</button>
           </form>
           <div class = 'prolist'> 
              <div class="col-12 col-md-7">
                    <h4 style ="margin-top:30px;margin-bottom:30px;">Create Items</h4>
                    <form>
                       <label>Box Number</label>
                       <input type="text" name = "boxnumber" id='boxnumber' placeholder="Box Number" autocomplete="off" /><br>
                       <label>Size</label>
                       <select name = "size" id = 'size'>
                          <option value="3ply">3 Play</option>
                          <option value="4ply">4 Ply</option>
                          <option value="6ply">6 Ply</option>
                          <option value="8ply">8 Ply</option>
                          <option value="9ply">9 Ply</option>
                          <option value="16ply">16 Ply</option>
                       </select><br>
                       <label>Bag Date</label>
                       <input id = "bagdate" type="date" placeholder="Date"><br>
                       <label>Net Weight</label>
                       <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id = 'netwt' placeholder="Net Weight" autocomplete="off" /><br>
                       <label>Gross Weight</label>
                       <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id = 'grosswt' placeholder="Gross Weight" autocomplete="off"/><br>
                       <input onclick="store_bags()" type="button" value="GENERATE"/><br>
                       <input type="reset" value="RESET" /><br>
                    </form>
                    
              </div>
              <div class="col-12 col-md-5"> 
                <h4 style ="margin :30px;">QR Code</h4>
                    <div class = 'img' id = 'img'>
                       <img id = 'qrcode'>
                       <canvas id="canvas"></canvas>
                    </div>
                   <button onclick = "imagecheck()">PRINT</button>
              </div>
            </div>
           <!------------- submit status -------------->
              <div class="alert alert-success alert-dismissible fade in submitstatus" id = 'submitstatus'>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong><span id = "submitmsg"></span>
              </div>
          <!------------- submit status -------------->
        </div>

        <!--- view all bags --->
        <div id="viewbags" class="tab-pane fade ">
             <div style = 'height: 80px;width: auto;'>
                <h4 style ="margin:20px 20px 20px 10px;float : left;">View</h4>
                <div class="btn-group" style = "margin:20px;float : right;">
                  <button onclick = "add_new_bag(0)" type="button" class="btn btn-primary">Add new bag</button>
                  <button onclick = "bagedittableviewbool()" type="button" class="btn btn-primary viewbtn3">Edit</button>
                  <button onclick = "bagdeletetableviewbool()" type="button" class="btn btn-primary viewbtn4">Delete</button>
                </div>
             </div>
             <div class = 'searchinputs'>
                <select id = "bagsize" onchange = "searchsizebags()">
                  <option>All Size</option>
                  <option value="3ply">3Play</option>
                  <option value="4ply">4Ply</option>
                  <option value="6ply">6Ply</option>
                  <option value="8ply">8Ply</option>
                  <option value="9ply">9Ply</option>
                  <option value="16ply">16Ply</option>
                </select>
                 <input onfocus="datechange(this)" onblur="textchange(this)" onchange="datesearchbags()" id="bagfromdate" type="text" placeholder="From date" autocomplete="off">
                  <input onfocus="datechange(this)" onblur="textchange(this)" onchange="datesearchbags()" id="bagtodate" type="text" placeholder="To date" autocomplete="off">
                  <input name="" onclick = "clear_dates('stock_boxes')" value = "Clear Dates" type = "button" class="btn-danger"/>
             </div>
            <div class = "boxes">
                <div class = "bagsinhand">
                    <i class="fas fa-cubes"></i>
                    <h3 id = 'bremain'></h3>
                    <p>BAGS IN HAND</p>
                </div>
                <div class = "bagsinhand">
                    <i class="fas fa-weight"></i>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">N</p>
                      <h4 id='bremainnetwt'></h4>
                    </div>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">G</p>
                      <h4 style="" id='bremaingrosswt'></h4>
                    </div>
                    <p>WEIGHTS IN HAND</p>
                </div>
                <div class = "bagssold">
                    <i class="fas fa-cubes"></i>
                    <h3 id = "bused"></h3>
                    <p>BAGS USED</p>
                </div>
                <div class = "bagssold">
                    <i class="fas fa-weight"></i>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">N</p>
                      <h4 id='busednetwt'></h4>
                    </div>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">G</p>
                      <h4 id="busedgrosswt"></h4>
                    </div>
                    <p>WEIGHTS USED</p>
                </div>
            </div>

            <div class="listbox">
                 
                <div class = "listdetails" style = "width : 350px;">
                  <p>Total Bags</p>
                  <h4 id = "viewbtbags" style="color: #2cc6de;"></h4>
                </div>
                <div class = "listdetails" style = "width : 350px;">
                  <p>Total Netweight</p>
                  <h4 id = "viewbtnetwt" style="color: #2cc6de;"></h4>
                </div>
                <div class = "listdetails" style = "width : 350px;">
                  <p>Total Grossweight</p>
                  <h4 id = "viewbtgrosswt" style="color: #2cc6de;"></h4>
                </div>
            </div>


            <table class="table table-bordered table-striped">
                  <thead>
                      <tr><th>Box Number</th><th>Size</th><th>Box Date</th><th>Net Weight</th><th>Gross Weight</th><th>Scanned Date</th><th>customer</th><th>QR Code</th><th>List ID</th></tr>
                  </thead>

                  <tbody id="viewbagstable">
                      <?php
                          $k = 0;$j = 0;$netwt = 0;$grosswt = 0;$tnetwt = 0;$tgrosswt = 0;
                          if($bagscheck > 0)
                          { 
                              while($row = mysqli_fetch_assoc($bagsresult))
                              {
                                echo "<tr onclick = 'edittableview(this)'><td>".$row['boxnumber']."</td><td>" .$row['size']."</td><td>".$row['box_date']."</td><td>" .$row['netweight']."</td><td>".$row['grossweight']."</td><td>".$row['scanneddate']."</td><td>".$row['customer']."</td><td><img width = 35 onclick='showqrcode(this)' src = "."data:image/jpg:chasrset-utf8;base64,".base64_encode($row['qrcode'])." /></td><td>".$row['uniqueid']."</td></tr>"; 

                                  $j++;
                                  $tnetwt += $row['netweight'];
                                  $tgrosswt += $row['grossweight'];

                                  if($row['scanneddate'] != '')
                                  {
                                      $k++;
                                      $netwt += $row['netweight'];
                                      $grosswt += $row['grossweight'];
                                  }
                              }
                          }
                      ?> 
                  </tbody>
            </table>
        </div>       

        <!--- view all bags end -->
        <div id="search" class="tab-pane fade ">
            <div class = "container" style="margin-bottom: 10px;">
              <h4 style ="margin:20px 20px 20px 0px;float : left;">Search</h4>
              <div class="btn-group" style = "margin:20px;float : right;">

                  <button onclick = "edittable()" type="button" class="btn btn-primary btn1">Edit</button>
                  <button onclick = "deletetable()" type="button" class="btn btn-primary btn2">Delete</button>
              </div>
            </div>
            <div class = 'searchinputs'>
                <select id = "ssize" onchange = "searchsize()">
                  <option>All Size</option>
                  <option value="3ply">3Play</option>
                  <option value="4ply">4Ply</option>
                  <option value="6ply">6Ply</option>
                  <option value="8ply">8Ply</option>
                  <option value="9ply">9Ply</option>
                  <option value="16ply">16Ply</option>
                </select>
                 <input onfocus="datechange(this)" onblur="textchange(this)" onchange="datesearch()" id="fromdate" type="text" placeholder="From date" autocomplete="off">
                  <input onfocus="datechange(this)" onblur="textchange(this)" onchange="datesearch()" id="todate" type="text" placeholder="To date" autocomplete="off">
                  <input name="" onclick = "clear_dates('stock')" value = "Clear Dates" type = "button" class="btn-danger"/>
            </div>
            <input class="form-control" id="myInput" type="text" placeholder="Search.."><br>
            <div class = "prosearch">
                <table class="table table-bordered table-striped">
                  <thead>
                      <tr><th>List Id</th><th>Production number</th><th>Size</th><th>Production Date</th><th>Bags</th><th>Total NetWeight</th><th>Total GrossWeight</th><th>Customers</th></tr>
                  </thead>

                  <tbody id="myTable">
                      <?php
                          if($check > 0)
                          { 
                              while($row = mysqli_fetch_assoc($result1))
                              {
                                echo "<tr ondblclick = 'table_view_doubleclick(this)' onclick = 'tablevalues(this)'><td>".$row['uniqueid']."</td><td>" .$row['stocknumber']."</td><td>".$row['size']."</td><td>".$row['stockdate']."</td><td>".$row['bags']."</td><td>".$row['totalnetweight']."</td><td>".$row['totalgrossweight']."</td><td>".$row['customer']."</td></tr>";
                              }
                          }
                      ?> 
                  </tbody>
                </table>

            </div>
        </div>
        <div id="view" class="tab-pane fade ">
             <div style = 'height: 70px;width: auto;'>
                <h4 style ="margin:20px 20px 20px 10px;float : left;">View</h4>
                <div class="btn-group" style = "margin:20px;float : right;">
                  <button onclick = "add_new_bag(1)" type="button" class="btn btn-primary">Add new bag</button>
                  <button onclick = "edittableviewbool()" type="button" class="btn btn-primary viewbtn1">Edit</button>
                  <button onclick = "deletetableviewbool()" type="button" class="btn btn-primary viewbtn2">Delete</button>
                </div>
            </div>
            <input style = 'margin-bottom: 20px;display: none;' type="text" class = 'form-control' placeholder="ID"  readonly="" />

            
            <div class = "boxes">
                <div class = "bagsinhand">
                    <i class="fas fa-cubes"></i>
                    <h3 id = 'remain'></h3>
                    <p>BAGS IN HAND</p>
                </div>
                <div class = "bagsinhand">
                    <i class="fas fa-weight"></i>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">N</p>
                      <h4 id='remainnetwt'></h4>
                    </div>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">G</p>
                      <h4 style="" id='remaingrosswt'></h4>
                    </div>
                    <p>WEIGHTS IN HAND</p>
                </div>
                <div class = "bagssold">
                    <i class="fas fa-cubes"></i>
                    <h3 id = "used"></h3>
                    <p>BAGS USED</p>
                </div>
                <div class = "bagssold">
                    <i class="fas fa-weight"></i>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">N</p>
                      <h4 id='usednetwt'></h4>
                    </div>
                    <div style = "display :flex;">
                      <p style="margin: 12px 10px 0px 0px;">G</p>
                      <h4 id="usedgrosswt"></h4>
                    </div>
                    <p>WEIGHTS USED</p>
                </div>
            </div>

            <div class="listbox">
                 <div class = "listdetails">
                  <p>List ID</p>
                  <h4 id = 'viewuid'></h4>
                </div>
                <div class = "listdetails">
                  <p>Production Number</p>
                  <h4 id = "viewpno"></h4>
                </div>
                <div class = "listdetails">
                  <p>Size</p>
                  <h4 id = "viewsize"></h4>
                </div>
                <div class = "listdetails">
                  <p>Production Date</p>
                  <h4 id = "viewdate"></h4>
                </div>
                <div class = "listdetails">
                  <p>Total Bags</p>
                  <h4 id = "viewtbags" style="color: #2cc6de;"></h4>
                </div>
                <div class = "listdetails">
                  <p>Total Netweight</p>
                  <h4 id = "viewtnetwt" style="color: #2cc6de;"></h4>
                </div>
                <div class = "listdetails">
                  <p>Total Grossweight</p>
                  <h4 id = "viewtgrosswt" style="color: #2cc6de;"></h4>
                </div>
            </div>


            <table class="table table-bordered table-striped">
                  <thead>
                      <tr><th>Box Number</th><th>Size</th><th>Box Date</th><th>Net Weight</th><th>Gross Weight</th><th>Scanned Date</th><th>customer</th><th>QR Code</th></tr>
                  </thead>

                  <tbody id="viewtable">
                      
                  </tbody>
            </table>
        </div>       
    </div>
</div>

<!---- modal --->

<!-- Modal edit start -->
                    <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Entry</h4>
                          </div>
                          <div class="modal-body">
                              <form> 
                                  <div class="form-group row">
                                      <label class = "col-sm-2 col-form-label">Unique ID</label>
                                      <input type="text" placeholder="Unique ID" id="euid" readonly="" /><br>
                                  </div>
                                  <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Size</label>
                                      <select id = 'elsize'>
                                        <option value="3ply">3 Play</option>
                                        <option value="4ply">4 Ply</option>
                                        <option value="6ply">6 Ply</option>
                                        <option value="8ply">8 Ply</option>
                                        <option value="9ply">9 Ply</option>
                                        <option value="16ply">16 Ply</option>
                                    </select>
                                </div>
                                  <div class="form-group row">
                                     <label class = "col-sm-2 col-form-label">Production Number</label>
                                     <input type="text" name = "ordernumber" placeholder="Production Number" id="eprono"/><br>
                                  </div>
                                  <div class="form-group row">
                                     <label class = "col-sm-2 col-form-label">Production Date</label>
                                     <input type="date" id="epdate"/><br>
                                  </div>
                              </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick  = "editsubmit()" data-dismiss="modal">Update</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!---- modal edit end -->
<!-- Modal delete start -->
                    <div class="modal fade" id="myModal2" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete</h4>
                          </div>
                          <div class="modal-body">
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Unique ID</label>
                                   <input type="text" name = "uniqueid" placeholder="uniqueid" id="duniqueid"/><br>
                                   <h4 style = "margin:20px;"> Do You want to delete this list ? </h4>
                                </div>
                          </div>
                          <div class="modal-footer">
                           <button type="button"class="btn btn-danger"onclick="deletesubmit()"data-dismiss="modal">Delete</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!----- modal delete end -->
<!-- Modal add start -->
                    <div class="modal fade" id="myModal3" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Items(Bags)</h4>
                          </div>
                          <div class="modal-body">
                              <form> 
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Box Number</label>
                                   <input type="text" placeholder="Box Number" id="aboxnumber" autocomplete="off" /><br> 
                                </div>
                                <div class="form-group row">
                                    <label class = "col-sm-2 col-form-label">Size</label>
                                    <select id = 'asize'>
                                        <option value="3ply">3 Play</option>
                                        <option value="4ply">4 Ply</option>
                                        <option value="6ply">6 Ply</option>
                                        <option value="8ply">8 Ply</option>
                                        <option value="9ply">9 Ply</option>
                                        <option value="16ply">16 Ply</option>
                                    </select>    
                                </div>

                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Net Weight</label>
                                   <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Net Weight" id="anetwt" autocomplete="off" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Gross Weight</label>
                                   <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Gross Weight" id="agrosswt" autocomplete="off" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Date</label>
                                   <input type="date" id="adate"/><br>
                                </div>
                                
                              </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick  = "add_new_item_verification()" data-dismiss="modal">ADD</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!---- modal add end -->
<!-- Modal view edit start -->
                    <div class="modal fade" id="myModal4" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Entry</h4>
                          </div>
                          <div class="modal-body">
                              <form> 
                                <div class="form-group row">
                                   <label class = "col-sm-4 col-form-label">Unique ID</label>
                                   <input type="text" placeholder="Unique ID" id="veuid" readonly="" /><br>
                                </div>
                                <hr>
                                <div class="form-group row">
                                   <label class = "col-sm-4 col-form-label">Box Number</label>
                                   <input type="text" placeholder="Production Number" id="veboxnumber" autocomplete="off" readonly=""/><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-4 col-form-label">Size</label>
                                      <select id = 'vesize'>
                                        <option value="3ply">3 Play</option>
                                        <option value="4ply">4 Ply</option>
                                        <option value="6ply">6 Ply</option>
                                        <option value="8ply">8 Ply</option>
                                        <option value="9ply">9 Ply</option>
                                        <option value="16ply">16 Ply</option>
                                    </select><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-4 col-form-label">Box Date</label>
                                   <input type="date" id="vedate"/><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-4 col-form-label">Net Weight</label>
                                   <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Net Weight" id="venetwt" autocomplete="off" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-4 col-form-label">Gross Weight</label>
                                   <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Gross Weight" id="vegrosswt" autocomplete="off" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-4 col-form-label">Scanned Date</label>
                                   <input type="Date" id="vescanneddate" style = "width : 150px;"/>
                                   <button type="button" class="btn btn-danger" style = "width : 95px;height: 32px;" onclick = "remove_sdate()">Remove</button>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-4 col-form-label">Customer</label>
                                    <select onchange='customer("vescustomer")' id = 'vescustomer'>
                                          <option>Customer</option>
                                          <option>New</option>
                                          <?php
                                              $i =0;
                                              while($i < count($customers))
                                              {
                                                 echo "<option>".$customers[$i]."</option>";
                                                 $i++;
                                              }  
                                          ?>
                                   </select>
                                </div>
                              </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick  = "vieweditsubmit()" data-dismiss="modal">Update</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!---- modal view edit end -->
<!-- Modal bag edit start -->
                    <div class="modal fade" id="myModal7" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Bag Edit Entry</h4>
                          </div>
                          <div class="modal-body">
                              <form> 
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Unique ID</label>
                                   <input type="text" placeholder="Unique ID" id="bveuid" readonly="" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Box Number</label>
                                   <input type="text" placeholder="Production Number" id="bveboxnumber" autocomplete="off"/><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Size</label>
                                      <select id = 'bvesize'>
                                        <option value="3ply">3 Play</option>
                                        <option value="4ply">4 Ply</option>
                                        <option value="6ply">6 Ply</option>
                                        <option value="8ply">8 Ply</option>
                                        <option value="9ply">9 Ply</option>
                                        <option value="16ply">16 Ply</option>
                                    </select><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Box Date</label>
                                   <input type="date" id="bvedate"/><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Net Weight</label>
                                   <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Net Weight" id="bvenetwt" autocomplete="off" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Gross Weight</label>
                                   <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Gross Weight" id="bvegrosswt" autocomplete="off" /><br>
                                </div>
                              </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" onclick  = "bagvieweditsubmit()" data-dismiss="modal">Update</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!---- modal bag edit end -->                    
<!-- Modal view delete start -->
                    <div class="modal fade" id="myModal5" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete Bags</h4>
                          </div>
                          <div class="modal-body">
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Unique ID</label>
                                   <input type="text" placeholder="uniqueid" id="vduniqueid" readonly="" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Box number</label>
                                   <input type="text" placeholder="Box Number" id="vdboxnumber" readonly="" /><br>
                                </div>
                                <h4 style = "margin:20px;"> Do You want to delete this bag ? </h4>
                          </div>
                          <div class="modal-footer">
                           <button type="button"class="btn btn-danger"onclick="deletetableviewsubmit()"data-dismiss="modal">Delete</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!----- modal view delete end -->
<!-- Modal qrcode show -->
                    <div class="modal fade" id="myModal6" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">QRCODE</h4>
                          </div>
                          <div class="modal-body">
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Unique ID</label>
                                   <input type="text" placeholder="uniqueid" id='codeid' readonly="" /><br>
                                </div>
                                <div class="form-group row">
                                   <label class = "col-sm-2 col-form-label">Box number</label>
                                   <input type="text" placeholder="Box Number" id='codeno' readonly="" /><br>
                                </div>
                                <div id = "modalimgdiv"><img style="margin-left: 150px;" id = "modalimg" /></div>
                          </div>
                          <div class="modal-footer">
                            <button type="button"class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="button"class="btn btn-primary" onclick = "imagecheck2()">Print</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!----- modal qrcode show end -->
<!---- modals end -->

</body>
</html>

<style>
  
</style>

<script>

document.getElementById('bremain').innerHTML=<?php echo $j-$k;?>;
document.getElementById('bremainnetwt').innerHTML = <?php echo $tnetwt - $netwt; ?>;
document.getElementById('bremaingrosswt').innerHTML = <?php echo $tgrosswt -$grosswt; ?>;
document.getElementById('bused').innerHTML = <?php echo $k; ?>;
document.getElementById('busednetwt').innerHTML = <?php echo $netwt; ?>;
document.getElementById('busedgrosswt').innerHTML = <?php echo $grosswt; ?>;
document.getElementById('viewbtbags').innerHTML = <?php echo $j; ?>;
document.getElementById('viewbtnetwt').innerHTML = <?php echo $tnetwt; ?>;
document.getElementById('viewbtgrosswt').innerHTML = <?php echo $tgrosswt; ?>;

$('#submitstatus').delay(5000).hide(0); 

// list creation.
var listbool = false;
function createlist()
{
   if(listbool == false)
   {
      let msg = '';
      if(document.getElementById('lcustomer').value == 'Customer')
      {
          msg += '\n◉ Select the customer';
      }
      if(document.getElementById('date').value == '')
      {
          msg += '\n◉ Select the Date';
      }
      if(document.getElementById('pno').value == '')
      {
          msg += '\n◉ Enter the Purchase Number';
      }
      if(document.getElementById('lsize').value == '')
      {
          msg = '◉ Select the size';
      }  
      if(msg != '')
      {
         alert(msg);
      }
      else
      {
          listbool = true;
          $('.idbtn').removeClass('btn-primary').addClass('btn-danger');
          document.querySelector('.idbtn').innerHTML = 'Close List';
          let d = new Date();
          let id = d.getFullYear()+''+d.getMonth()+''+d.getDate()+''+d.getHours()+''+d.getMinutes()+''+d.getSeconds();
          document.getElementById('unid').value = id;
          store_list();
      }
   }
   else
   {
      listbool = false;
      $('.idbtn').removeClass('btn-danger').addClass('btn-primary');
      document.querySelector('.idbtn').innerHTML = 'Create New List';
      document.getElementById('unid').value = '';
      document.getElementById('pno').value = '';
      document.getElementById('date').value ='';
      document.getElementById('lsize').value ='3ply';
      var canvas = document.getElementById('canvas'),
      ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, canvas.width, canvas.height);
   }
}

// create list will store in database
function store_list()
{
    let pcustomer = document.getElementById('lcustomer').value;
    let lsize = document.getElementById('lsize').value;
    let uid = document.getElementById('unid').value;
    let pronum = document.getElementById('pno').value;
    let date = document.getElementById('date').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
           if(this.responseText == 'success')
           {
              document.getElementById('submitmsg').innerHTML = "The production list was successfully created.";
              document.getElementById('submitstatus').style.display = 'block';
               $('#submitstatus').delay(5000).hide(0); 

           }
           else
           {
              alert('Purchase list was not created.');
              listbool = false;
              $('.idbtn').removeClass('btn-danger').addClass('btn-primary');
              document.querySelector('.idbtn').innerHTML = 'Create New List';
              document.getElementById('unid').value = "";
           }
        }
    };
    xhttp.open("POST", "phpback/create_list.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("customer="+pcustomer+"&size="+lsize+"&uniqueid="+uid+"&number="+pronum+"&date="+date+"&db=stock"); 
}

// generate qr code
function generate_code(uid,boxno,size,key)
{
    var image = new Image();
    image.src = "phpback/qrimages/"+uid+"-"+boxno+".png";
    var canvas = document.getElementById('canvas'),
    ctx = canvas.getContext('2d');
    canvas.width = 250;
    canvas.crossOrigin = "Anonymous";
    canvas.height = 250;
    ctx.font = "10pt Verdana";
    image.onload = function()
    {
        ctx.drawImage(image, 0, 0);
        ctx.fillStyle = "red";
        ctx.fillText(boxno,35,canvas.height-30);
        ctx.fillStyle = "green";
        ctx.fillText('3ply',canvas.width-85,canvas.height-30);
        document.getElementById('qrcode').src = canvas.toDataURL();
        qrcode_submit(uid,boxno,canvas.toDataURL("image/png"),key);
    }
    if(key == 1){canvas.style.display='none';}else{canvas.style.display='block';} 
} 

function qrcode_submit(uid,boxno,qrcode,key)
{
     var xhttp = new XMLHttpRequest();
     xhttp.onreadystatechange = function() 
     {
        if(this.readyState == 4 && this.status == 200)
        {
             var parts = this.responseText.split('$');
             if(parts[0] == 'submit')
             {  
                 if(key == 1)
                 {
                    if(bagkey == 0)
                    {
                        document.getElementById("viewbagstable").innerHTML = parts[1];
                        document.getElementById("viewbtbags").innerHTML = parts[2];
                        document.getElementById("viewbtnetwt").innerHTML = parts[3];
                        document.getElementById("viewbtgrosswt").innerHTML = parts[4];
                        document.getElementById("bremain").innerHTML = parts[2]-parts[5];
                        document.getElementById("bused").innerHTML = parts[5];
                        document.getElementById("bremainnetwt").innerHTML = parts[3]-parts[6];
                        document.getElementById("bremaingrosswt").innerHTML = parts[4]-parts[7];
                        document.getElementById("busednetwt").innerHTML = parts[6];
                        document.getElementById("busedgrosswt").innerHTML = parts[7];
                    }
                    else
                    {
                        document.getElementById("viewtable").innerHTML = parts[1];
                        document.getElementById("viewtbags").innerHTML = parts[2];
                        document.getElementById("viewtnetwt").innerHTML = parts[3];
                        document.getElementById("viewtgrosswt").innerHTML = parts[4];
                        document.getElementById("remain").innerHTML = parts[2]-parts[5];
                        document.getElementById("used").innerHTML = parts[5];
                        document.getElementById("remainnetwt").innerHTML = parts[3]-parts[6];
                        document.getElementById("remaingrosswt").innerHTML = parts[4]-parts[7];
                        document.getElementById("usednetwt").innerHTML = parts[6];
                        document.getElementById("usedgrosswt").innerHTML = parts[7];
                    }
                }
             }
             else{
                //
             }
        }
     };
 
     xhttp.open("POST", "phpback/update_qrcode.php", true);
     xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
     xhttp.send("key="+key+"&bagkey="+bagkey+"&uniqueid="+uid+"&boxnumber="+boxno+"&qrcode="+qrcode+"&db=stock_boxes&bdb=stock");
}

// items verification 
function items_verification(boxno,size,date,netwt,grosswt)
{
      let msg = '';
      if(boxno == '')
      {
          msg = '◉ Enter Box Number';
      }  
      if(size == '')
      {
          msg += '\n◉ Select the Size';
      }
      if(date == '')
      {
          msg += '\n◉ Select the Date';
      }
      if(netwt == '')
      {
          msg += '\n◉ Enter the Net Weight';
      }
      if(grosswt == '')
      {
          msg += '\n◉ Enter the Gross Weight';
      }
      if(msg != '')
      {
         alert(msg);
         return false;
      }
      else
      {
        return true;
      }
}

// store bags in list
function store_bags()
{
    let uid = document.getElementById('unid').value;
    let boxno = document.getElementById('boxnumber').value;
    let size = document.getElementById('size').value;
    let date = document.getElementById('bagdate').value;
    let netwt = document.getElementById('netwt').value;
    let grosswt = document.getElementById('grosswt').value;
    if(items_verification(boxno,size,date,netwt,grosswt))
    {
       if(networkcheck())
       {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() 
          {
              if(this.readyState == 4 && this.status == 200)
              {
                 var parts = this.responseText.split('$');
                 if(parts[0] == 'success')
                 {
                    document.getElementById('submitmsg').innerHTML="The production bag was successfully added.";
                    document.getElementById('submitstatus').style.display = 'block';
                    document.getElementById('boxnumber').value = '';
                    document.getElementById('bagdate').value = '';
                    document.getElementById('size').value = '3ply';
                    document.getElementById('netwt').value = '';
                    document.getElementById('grosswt').value = '';
                    generate_code(uid,boxno,size,0);
                    $('#submitstatus').delay(5000).hide(0); 
                 }
                 else if(parts[0] == 'exist')
                 {
                    alert("Box Number already exist in this list.");
                 }
                 else
                 {
                    alert("QRCODE was not generated.");
                    //alert(parts[0]);
                 }
              }
          };
          xhttp.open("POST", "phpback/store_bags.php", true);
          xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhttp.send("key=0&uniqueid="+uid+"&boxnumber="+boxno+"&size="+size+"&date="+date+"&netwt="+netwt+"&grosswt="+grosswt+"&db=stock_boxes&bdb=stock");
       }
       else
       {
            alert("No internet available to generate qrcode.");
       }
    }
  
}

// table view , edit , delete in search.
function tablevalues(val)
{
    let id = val.childNodes[0].innerHTML;  
    let pno = val.childNodes[1].innerHTML;
    let size = val.childNodes[2].innerHTML;
    let pdate = val.childNodes[3].innerHTML;
    let bags = val.childNodes[4].innerHTML; 
    let totalnetwt = val.childNodes[5].innerHTML;
    let totalgrosswt = val.childNodes[6].innerHTML;  
    
    if(editbool == true || deletebool == true)
    {
        if(editbool == true)
        {
            document.getElementById('euid').value = val.childNodes[0].innerHTML;
            document.getElementById('eprono').value = val.childNodes[1].innerHTML;
            document.getElementById('elsize').value = val.childNodes[2].innerHTML;
            document.getElementById('epdate').value = val.childNodes[3].innerHTML;

            $('#myModal').modal('show');
        }
        else
        {    document.getElementById('duniqueid').value = val.childNodes[0].innerHTML;
             $('#myModal2').modal('show');  
        }
    }
    /*else
    {
        var xhttp;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() 
        {
          if(this.readyState == 4 && this.status == 200)
          {
              var parts = this.responseText.split('$');
              document.getElementById("viewuid").innerHTML = id;
              document.getElementById("viewpno").innerHTML = pno;
              document.getElementById("viewsize").innerHTML = size;
              document.getElementById("viewdate").innerHTML = pdate;
              document.getElementById("viewtbags").innerHTML = bags;
              document.getElementById("viewtnetwt").innerHTML = totalnetwt;
              document.getElementById("viewtgrosswt").innerHTML = totalgrosswt;

              document.getElementById("viewtable").innerHTML = parts[0];
              document.getElementById("remain").innerHTML = bags-parts[1];
              document.getElementById("used").innerHTML = parts[1];
              document.getElementById("remainnetwt").innerHTML = totalnetwt-parts[2];
              document.getElementById("remaingrosswt").innerHTML = totalgrosswt-parts[3];
              document.getElementById("usednetwt").innerHTML = parts[2];
              document.getElementById("usedgrosswt").innerHTML = parts[3];
          }
        };
        xhttp.open("GET", "phpback/searchview.php?id="+id+"&type=stock_boxes", true);
        xhttp.send(); 
        document.getElementById("viewid").click();
    }  */
}

function table_view_doubleclick(val)
{
    let id = val.childNodes[0].innerHTML;  
    let pno = val.childNodes[1].innerHTML;
    let size = val.childNodes[2].innerHTML;
    let pdate = val.childNodes[3].innerHTML;
    let bags = val.childNodes[4].innerHTML; 
    let totalnetwt = val.childNodes[5].innerHTML;
    let totalgrosswt = val.childNodes[6].innerHTML;  
    var xhttp;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() 
        {
          if(this.readyState == 4 && this.status == 200)
          {
              var parts = this.responseText.split('$');
              document.getElementById("viewuid").innerHTML = id;
              document.getElementById("viewpno").innerHTML = pno;
              document.getElementById("viewsize").innerHTML = size;
              document.getElementById("viewdate").innerHTML = pdate;
              document.getElementById("viewtbags").innerHTML = bags;
              document.getElementById("viewtnetwt").innerHTML = totalnetwt;
              document.getElementById("viewtgrosswt").innerHTML = totalgrosswt;

              document.getElementById("viewtable").innerHTML = parts[0];
              document.getElementById("remain").innerHTML = bags-parts[1];
              document.getElementById("used").innerHTML = parts[1];
              document.getElementById("remainnetwt").innerHTML = totalnetwt-parts[2];
              document.getElementById("remaingrosswt").innerHTML = totalgrosswt-parts[3];
              document.getElementById("usednetwt").innerHTML = parts[2];
              document.getElementById("usedgrosswt").innerHTML = parts[3];
          }
        };
        xhttp.open("GET", "phpback/searchview.php?id="+id+"&type=stock_boxes", true);
        xhttp.send(); 
        document.getElementById("viewid").click();
}

// table edit submit in search.
function editsubmit()
{
    var uid =  document.getElementById('euid').value;
    var pronum =  document.getElementById('eprono').value;
    var date = document.getElementById('epdate').value;
    var size = document.getElementById('elsize').value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200)
     {
        var parts = this.responseText.split('$');
        if(parts[1] != 'false')
        {
            document.getElementById("ssize").value = "All Size";
            document.getElementById("myTable").innerHTML = parts[0];
        }
        else
        {
           $('#myModal').modal('hide');  
           alert('Not updated.');
        }
      }
    };
    xhttp.open("POST", "phpback/update.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   xhttp.send("uid="+uid+"&size="+size+"&num="+pronum+"&date="+date+"&db=stock");
}

// table delete submit in search.
function deletesubmit()
{
    var uniqueid = document.getElementById('duniqueid').value;
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200)
      {
          var parts = this.responseText.split('$');
          if(parts[1] == 'false')
          {
              document.getElementById("ssize").value = "All Size";
              document.getElementById("myTable").innerHTML = '';
          }
          else{
              document.getElementById("ssize").value = "All Size";
              document.getElementById("myTable").innerHTML = parts[0];
          }
      }
    };
    xhttp.open("POST", "phpback/delete.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("uniqueid="+uniqueid+"&db=stock&sdb=stock_boxes");
}

// add new item verificetion
function add_new_item_verification()
{
      let msg = '';
      if(document.getElementById('aboxnumber').value == '')
      {
          msg = '◉ Enter Box Number';
      }  
      if(document.getElementById('asize').value == '')
      {
          msg += '\n◉ Select the Size';
      }
      if(document.getElementById('adate').value == '')
      {
          msg += '\n◉ Select the Date';
      }
      if(document.getElementById('anetwt').value == '')
      {
          msg += '\n◉ Enter the Net Weight';
      }
      if(document.getElementById('agrosswt').value == '')
      {
          msg += '\n◉ Enter the Gross Weight';
      }
      if(msg != '')
      {
         alert(msg);
      }
      else
      {
         add_bag_submit();
      }
}

var bagkey;
// add new item in list view.
function add_new_bag(key)
{
    if(document.getElementById('viewuid').innerHTML != '' || key == 0)
    {
      bagkey = key;
      $('#myModal3').modal('show');
    }
    else
    {
      alert("Select the list First then add new Item(Bag).");
    }
}

// add new item in list view submit.
function add_bag_submit()
{ 
    if(bagkey == 1)
    {
      var uid = document.getElementById('viewuid').innerHTML;
    }
    else
    {
      var uid = "";
    }
    let boxno = document.getElementById('aboxnumber').value;
    let size = document.getElementById('asize').value;
    let date = document.getElementById('adate').value;
    let netwt = document.getElementById('anetwt').value;
    let grosswt = document.getElementById('agrosswt').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
           var parts = this.responseText.split('$');
           if(parts[0] == 'success')
           {
              generate_code(uid,boxno,size,1);
           }
           else if(parts[0] == 'exist')
           {
              $('#myModal3').modal('hide');
              alert("Box Number already exist in this list.");
           }
           else{
              $('#myModal3').modal('hide');
              alert("QRCODE not generated.");
           }
        }
    };
    xhttp.open("POST", "phpback/store_bags.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("key=1&uniqueid="+uid+"&boxnumber="+boxno+"&size="+size+"&date="+date+"&netwt="+netwt+"&grosswt="+grosswt+"&db=stock_boxes&bdb=stock");
  
}

var editviewbool = false;var bageditviewbool = false;
function edittableviewbool()
{
    if(document.getElementById('viewuid').innerHTML != '')
    {
        if(editviewbool == true)
        {
           editviewbool = false;
           $('.viewbtn1').removeClass('btn-danger').addClass('btn-primary ');
        }
        else
        {
           editviewbool = true;
           $('.viewbtn1').removeClass('btn-primary').addClass('btn-danger');
        }
    }
    else
    {
      alert("Select the list First then edit Item(Bag).");
    }
}
function bagedittableviewbool()
{
    if(bageditviewbool == true)
    {
        bageditviewbool = false;
        $('.viewbtn3').removeClass('btn-danger').addClass('btn-primary ');
    }
    else
    {
        bageditviewbool = true;
        $('.viewbtn3').removeClass('btn-primary').addClass('btn-danger');
    }
}

function edittableview(val)
{
    if(editviewbool == true)
    {
        document.getElementById('veuid').value = document.getElementById('viewuid').innerHTML;
        document.getElementById('veboxnumber').value = val.childNodes[0].innerHTML;
        document.getElementById('vesize').value = val.childNodes[1].innerHTML;
        document.getElementById('vedate').value = val.childNodes[2].innerHTML;
        document.getElementById('venetwt').value = val.childNodes[3].innerHTML;
        document.getElementById('vegrosswt').value = val.childNodes[4].innerHTML;
        document.getElementById('vescanneddate').value = val.childNodes[5].innerHTML;
        if(val.childNodes[6].innerHTML == "")
        {
            document.getElementById('vescustomer').value = "Customer";
        }
        else
        {
            document.getElementById('vescustomer').value = val.childNodes[6].innerHTML;
        }
        $('#myModal4').modal('show');
    }
    else if(bageditviewbool == true)
    {
        document.getElementById('bveuid').value = val.childNodes[8].innerHTML;
        document.getElementById('bveboxnumber').value = val.childNodes[0].innerHTML;
        document.getElementById('bvesize').value = val.childNodes[1].innerHTML;
        document.getElementById('bvedate').value = val.childNodes[2].innerHTML;
        document.getElementById('bvenetwt').value = val.childNodes[3].innerHTML;
        document.getElementById('bvegrosswt').value = val.childNodes[4].innerHTML;
        $('#myModal7').modal('show');
    }
    else if(deleteviewbool == true)
    {
        bagdeletekey = 1;
        document.getElementById('vduniqueid').value = document.getElementById('viewuid').innerHTML;
        document.getElementById('vdboxnumber').value = val.childNodes[0].innerHTML;
        netweight_old = val.childNodes[3].innerHTML;
        grossweight_old = val.childNodes[4].innerHTML;
        $('#myModal5').modal('show');
    }
    else if(bagdeleteviewbool == true)
    {
        bagdeletekey = 0;
        document.getElementById('vduniqueid').value = val.childNodes[8].innerHTML;
        document.getElementById('vdboxnumber').value = val.childNodes[0].innerHTML;
        netweight_old = val.childNodes[3].innerHTML;
        grossweight_old = val.childNodes[4].innerHTML;
        $('#myModal5').modal('show');
    }
    else
    {
        document.getElementById('codeid').value = document.getElementById('viewuid').innerHTML;
        document.getElementById('codeno').value = val.childNodes[0].innerHTML;
    }
}

function vieweditsubmit()
{
    let uid = document.getElementById('viewuid').innerHTML;
    let boxno = document.getElementById('veboxnumber').value;
    let size = document.getElementById('vesize').value;
    let date = document.getElementById('vedate').value;
    let netwt = document.getElementById('venetwt').value;
    let grosswt = document.getElementById('vegrosswt').value;
    let sdate = document.getElementById('vescanneddate').value;
    if(document.getElementById('vescustomer').value == "Customer")
    {
       var customer = "";
    }
    else
    {
       var customer = document.getElementById('vescustomer').value;
    }
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
           var parts = this.responseText.split('$');
           if(parts[1] == 'notsubmit')
           {
              $('#myModal4').modal('hide');
              alert("Not updated.");
           }
           else if(parts[1] == 'false')
           {
              alert("table not loaded");
           }
           else{
               document.getElementById("viewtable").innerHTML = parts[0];
                document.getElementById("viewtnetwt").innerHTML = parts[2];
                document.getElementById("viewtgrosswt").innerHTML = parts[3];
                
                document.getElementById("remain").innerHTML = parts[1]-parts[4];
                document.getElementById("used").innerHTML = parts[4];
                document.getElementById("remainnetwt").innerHTML = parts[2]-parts[5];
                document.getElementById("remaingrosswt").innerHTML = parts[3]-parts[6];
                document.getElementById("usednetwt").innerHTML = parts[5];
                document.getElementById("usedgrosswt").innerHTML = parts[6];
           }
        }
    };
    xhttp.open("POST", "phpback/update_bags.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("uid="+uid+"&bageditkey=1&boxnumber="+boxno+"&size="+size+"&date="+date+"&netwt="+netwt+"&grosswt="+grosswt+"&sdate="+sdate+"&customer="+customer+"&db=stock_boxes&bdb=stock"); 
}

function bagvieweditsubmit()
{
    let uid = document.getElementById('bveuid').value;
    let boxno = document.getElementById('bveboxnumber').value;
    let size = document.getElementById('bvesize').value;
    let date = document.getElementById('bvedate').value;
    let netwt = document.getElementById('bvenetwt').value;
    let grosswt = document.getElementById('bvegrosswt').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
           var parts = this.responseText.split('$');
           if(parts[1] == 'notsubmit')
           {
              $('#myModal7').modal('hide');
              alert("Not updated.");
           }
           else if(parts[1] == 'false')
           {
              alert("table not loaded");
           }
           else
           {
                document.getElementById("viewbagstable").innerHTML = parts[0];
                document.getElementById("viewbtbags").innerHTML = parts[1];
                document.getElementById("viewbtnetwt").innerHTML = parts[2];
                document.getElementById("viewbtgrosswt").innerHTML = parts[3];
                document.getElementById("bremain").innerHTML = parts[1]-parts[4];
                document.getElementById("bused").innerHTML = parts[4];
                document.getElementById("bremainnetwt").innerHTML = parts[2]-parts[5];
                document.getElementById("bremaingrosswt").innerHTML = parts[3]-parts[6];
                document.getElementById("busednetwt").innerHTML = parts[5];
                document.getElementById("busedgrosswt").innerHTML = parts[6];
           }
        }
    };
    xhttp.open("POST", "phpback/update_bags.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("uid="+uid+"&bageditkey=0&boxnumber="+boxno+"&size="+size+"&date="+date+"&netwt="+netwt+"&grosswt="+grosswt+"&customer=&sdate=&db=stock_boxes&bdb=stock"); 
}

var deleteviewbool = false; var bagdeleteviewbool = false;var bagdeletekey; 
function bagdeletetableviewbool()
{
   if(bagdeleteviewbool == true)
   {
       bagdeleteviewbool = false;
       $('.viewbtn4').removeClass('btn-danger').addClass('btn-primary ');
   }
   else
   {
       bagdeleteviewbool = true;
       $('.viewbtn4').removeClass('btn-primary').addClass('btn-danger');
   }
}
function deletetableviewbool()
{
  if(document.getElementById('viewuid').innerHTML != '')
    {
        if(deleteviewbool == true)
        {
           deleteviewbool = false;
           $('.viewbtn2').removeClass('btn-danger').addClass('btn-primary ');
        }
        else
        {
           deleteviewbool = true;
           $('.viewbtn2').removeClass('btn-primary').addClass('btn-danger');
        }
    }
    else
    {
      alert("Select the list First then delete Item(Bag).");
    }
}

var netweight_old,grossweight_old; // to update weights while deleting
function deletetableviewsubmit()
{
    let uniqueid = document.getElementById('vduniqueid').value;
    let bagnumber = document.getElementById('vdboxnumber').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            var parts = this.responseText.split('$');
            if(parts[1] == 'notdelete')
            {
                alert('Item(Bag) not deleted.');
            }
            else if(parts[1] == 'false')
            {
               if(bagdeletekey == 0)
               {
                  document.getElementById("viewbagstable").innerHTML = '';
                  document.getElementById("viewbtbags").innerHTML = 0;
                  document.getElementById("viewbtnetwt").innerHTML = 0;
                  document.getElementById("viewbtgrosswt").innerHTML = 0;
                  document.getElementById("bremain").innerHTML = 0;
                  document.getElementById("bused").innerHTML = 0;
                  document.getElementById("bremainnetwt").innerHTML = 0;
                  document.getElementById("bremaingrosswt").innerHTML = 0;
                  document.getElementById("busednetwt").innerHTML = 0;
                  document.getElementById("busedgrosswt").innerHTML = 0;
               }
               else
               {
                  document.getElementById("viewtable").innerHTML = '';
                  document.getElementById("viewtbags").innerHTML = 0;
                  document.getElementById("viewtnetwt").innerHTML = 0;
                  document.getElementById("viewtgrosswt").innerHTML = 0;
                  document.getElementById("remain").innerHTML = 0;
                  document.getElementById("used").innerHTML = 0;
                  document.getElementById("remainnetwt").innerHTML = 0;
                  document.getElementById("remaingrosswt").innerHTML = 0;
                  document.getElementById("usednetwt").innerHTML = 0;
                  document.getElementById("usedgrosswt").innerHTML = 0;
               }
            }
            else
            {
               if(bagdeletekey == 0)
               {
                  document.getElementById("viewbagstable").innerHTML = parts[0];
                  document.getElementById("viewbtbags").innerHTML = parts[1];
                  document.getElementById("viewbtnetwt").innerHTML = parts[2];
                  document.getElementById("viewbtgrosswt").innerHTML = parts[3];
                  document.getElementById("bremain").innerHTML = parts[1]-parts[4];
                  document.getElementById("bused").innerHTML = parts[4];
                  document.getElementById("bremainnetwt").innerHTML = parts[2]-parts[5];
                  document.getElementById("bremaingrosswt").innerHTML = parts[3]-parts[6];
                  document.getElementById("busednetwt").innerHTML = parts[5];
                  document.getElementById("busedgrosswt").innerHTML = parts[6];
               }
               else
               {
                  document.getElementById("viewtable").innerHTML = parts[0];
                  document.getElementById("viewtbags").innerHTML = parts[1];
                  document.getElementById("viewtnetwt").innerHTML = parts[2];
                  document.getElementById("viewtgrosswt").innerHTML = parts[3];
                  document.getElementById("remain").innerHTML = parts[1]-parts[4];
                  document.getElementById("used").innerHTML = parts[4];
                  document.getElementById("remainnetwt").innerHTML = parts[2]-parts[5];
                  document.getElementById("remaingrosswt").innerHTML = parts[3]-parts[6];
                  document.getElementById("usednetwt").innerHTML = parts[5];
                  document.getElementById("usedgrosswt").innerHTML = parts[6];
               }
            }
        }
    };
    xhttp.open("POST", "phpback/delete_bags.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("bagdeletekey="+bagdeletekey+"&uniqueid="+uniqueid+"&bagnumber="+bagnumber+"&oldnetwt="+netweight_old+"&oldgrosswt="+grossweight_old+"&db=stock_boxes&bdb=stock");
}

// search list with dates
function datesearch()
{
    let fdate = document.getElementById('fromdate').value;
    let tdate = document.getElementById('todate').value;
    if(fdate != '')
    {
        if(tdate == '')
        {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() 
            {
              if(this.readyState == 4 && this.status == 200)
              {
                  var parts = this.responseText.split('$');
                  if(parts[1] == 'false')
                  {
                      //
                  }
                  else
                  {
                      document.getElementById("myTable").innerHTML = parts[0];
                  }
              }
            };
            xhttp.open("GET", "phpback/search_dates.php?db=stock&fdate="+fdate, true);
            xhttp.send();
        }
        else
        {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() 
            {
              if(this.readyState == 4 && this.status == 200)
              {
                  var parts = this.responseText.split('$');
                  if(parts[1] == 'false')
                  {
                      //document.getElementById("myTable").innerHTML = parts[0];
                  }
                  else{
                      document.getElementById("myTable").innerHTML = parts[0];
                  }
              }
            };
            xhttp.open("GET", "phpback/search_dates.php?db=stock&fdate="+fdate+"&tdate="+tdate, true);
            xhttp.send();
        }
    }
    else{
      alert("You want to select from date also.");
      document.getElementById("todate").value = "";
    }
}


function imagecheck()
{
   const blank = document.createElement('canvas');

    blank.width = canvas.width;
    blank.height = canvas.height;

    if(canvas.toDataURL() === blank.toDataURL())
    {
      alert("No Qrcode.");
    }
    else
    {
        var contents = document.getElementById("img").innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame";
        frame1.style.position = "absolute";
        frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html><head><title>Qrcode</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
        window.frames["frame"].focus();
        window.frames["frame"].print();
        document.body.removeChild(frame1);
        }, 500);
    
    }
} 

function imagecheck2()
{
        var contents = document.getElementById("modalimgdiv").innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame";
        frame1.style.position = "absolute";
        frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html><head><title>Qrcode</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
        window.frames["frame"].focus();
        window.frames["frame"].print();
        document.body.removeChild(frame1);
        }, 500);
    
} 

function networkcheck()
  {
    if(navigator.onLine == true)
    {
       return true;
    }
    else
    {
       return false;
    }
  }

 function remove_sdate()
  {
     document.getElementById('vescanneddate').value = "";
     document.getElementById('vescustomer').value = "Customer";
  }

  // add new purchase customer 
function customer(id)
{
  if(document.getElementById(id).value == 'New')
  {
      let name = prompt('Create new customer');
      if(name != null && name != '')
      {
          let option = document.createElement("option");
          option.text = name;
         
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() 
          {
            if(this.readyState == 4 && this.status == 200)
            {
               if(this.responseText == 'exist')
               {
                  alert("Name already in database");
                  document.getElementById(id).value = option.text;
               }
               else if(this.responseText == 'true')
               {
                 document.getElementById(id).add(option);
                 document.getElementById(id).value = option.text;
               }
               else
               {
                 alert("Not created new customer.")
                 document.getElementById(id).value = 'Customer';
               }
            }
            document.getElementById(id).value = option.text;
          };
          xhttp.open("POST", "phpback/customer_post.php", true);
          xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhttp.send("name="+option.text+"&db=customers"); 
      }
      else{
         document.getElementById(id).value = 'Customer';
      }
  }
}

// search bags with size 
function searchsizebags()
{
  var str = document.getElementById("bagsize").value;
  var xhttp;
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() 
      {
          if(this.readyState == 4 && this.status == 200)
          {
            var parts = this.responseText.split('$');
            if(parts[1] == 'false' || parts[7] == 'false')
            {
                alert(str+" size not in database...");
                document.getElementById("bagsize").value = "All Size";
                document.getElementById("viewbagstable").innerHTML = parts[0];
                document.getElementById("viewbtbags").innerHTML = parts[1];
                document.getElementById("viewbtnetwt").innerHTML = parts[2];
                document.getElementById("viewbtgrosswt").innerHTML = parts[3];
                document.getElementById("bremain").innerHTML = parts[1]-parts[4];
                document.getElementById("bused").innerHTML = parts[4];
                document.getElementById("bremainnetwt").innerHTML = parts[2]-parts[5];
                document.getElementById("bremaingrosswt").innerHTML = parts[3]-parts[6];
                document.getElementById("busednetwt").innerHTML = parts[5];
                document.getElementById("busedgrosswt").innerHTML = parts[6];
            }
            else
            {
                document.getElementById("viewbagstable").innerHTML = parts[0];
                document.getElementById("viewbtbags").innerHTML = parts[1];
                document.getElementById("viewbtnetwt").innerHTML = parts[2];
                document.getElementById("viewbtgrosswt").innerHTML = parts[3];
                document.getElementById("bremain").innerHTML = parts[1]-parts[4];
                document.getElementById("bused").innerHTML = parts[4];
                document.getElementById("bremainnetwt").innerHTML = parts[2]-parts[5];
                document.getElementById("bremaingrosswt").innerHTML = parts[3]-parts[6];
                document.getElementById("busednetwt").innerHTML = parts[5];
                document.getElementById("busedgrosswt").innerHTML = parts[6];
            }
          }
      };
      xhttp.open("GET", "phpback/searchsize.php?db=stock_boxes&size="+str, true);
      xhttp.send();  
}


// search bags with dates
function datesearchbags()
{
    let fdate = document.getElementById('bagfromdate').value;
    let tdate = document.getElementById('bagtodate').value;
    if(fdate != '')
    {
        if(tdate == '')
        {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() 
            {
              if(this.readyState == 4 && this.status == 200)
              {
                  var parts = this.responseText.split('$');
                  if(parts[1] == 'false')
                  {
                      //
                  }
                  else
                  {
                      document.getElementById("viewbagstable").innerHTML = parts[0];
                      document.getElementById("viewbtbags").innerHTML = parts[1];
                      document.getElementById("viewbtnetwt").innerHTML = parts[2];
                      document.getElementById("viewbtgrosswt").innerHTML = parts[3];
                      document.getElementById("bremain").innerHTML = parts[1]-parts[4];
                      document.getElementById("bused").innerHTML = parts[4];
                      document.getElementById("bremainnetwt").innerHTML = parts[2]-parts[5];
                      document.getElementById("bremaingrosswt").innerHTML = parts[3]-parts[6];
                      document.getElementById("busednetwt").innerHTML = parts[5];
                      document.getElementById("busedgrosswt").innerHTML = parts[6];
                  }
              }
            };
            xhttp.open("GET", "phpback/search_dates.php?db=stock_boxes&fdate="+fdate, true);
            xhttp.send();
        }
        else
        {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() 
            {
              if(this.readyState == 4 && this.status == 200)
              {
                  var parts = this.responseText.split('$');
                  if(parts[1] == 'false')
                  {
                      //document.getElementById("myTable").innerHTML = parts[0];
                  }
                  else{
                      document.getElementById("viewbagstable").innerHTML = parts[0];
                      document.getElementById("viewbtbags").innerHTML = parts[1];
                      document.getElementById("viewbtnetwt").innerHTML = parts[2];
                      document.getElementById("viewbtgrosswt").innerHTML = parts[3];
                      document.getElementById("bremain").innerHTML = parts[1]-parts[4];
                      document.getElementById("bused").innerHTML = parts[4];
                      document.getElementById("bremainnetwt").innerHTML = parts[2]-parts[5];
                      document.getElementById("bremaingrosswt").innerHTML = parts[3]-parts[6];
                      document.getElementById("busednetwt").innerHTML = parts[5];
                      document.getElementById("busedgrosswt").innerHTML = parts[6];
                  }
              }
            };
            xhttp.open("GET", "phpback/search_dates.php?db=stock_boxes&fdate="+fdate+"&tdate="+tdate, true);
            xhttp.send();
        }
    }
    else{
      alert("You want to select from date also.");
      document.getElementById("bagtodate").value = "";
    }
}

// to clear the dates.
function clear_dates(db)
{

    document.getElementById('fromdate').value = '';
    document.getElementById('todate').value = '';
    document.getElementById('bagfromdate').value = '';
    document.getElementById('bagtodate').value = '';
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('$');
            if(db == "stock")
            {
                if(parts[1] == 'false')
                {
                    document.getElementById("myTable").innerHTML = parts[0];
                }
                else{
                    document.getElementById("myTable").innerHTML = parts[0];
                }
            }
            else
            {
                if(parts[1] == 'false')
                {
                    document.getElementById("viewbagstable").innerHTML = parts[0];
                    document.getElementById("viewbtbags").innerHTML = '';
                    document.getElementById("viewbtnetwt").innerHTML = '';
                    document.getElementById("viewbtgrosswt").innerHTML = '';
                    document.getElementById("bremain").innerHTML = '';
                    document.getElementById("bused").innerHTML = '';
                    document.getElementById("bremainnetwt").innerHTML = '';
                    document.getElementById("bremaingrosswt").innerHTML = '';
                    document.getElementById("busednetwt").innerHTML = '';
                    document.getElementById("busedgrosswt").innerHTML = '';
                }
                else
                {
                    document.getElementById("viewbagstable").innerHTML = parts[0];
                    document.getElementById("viewbtbags").innerHTML = parts[1];
                    document.getElementById("viewbtnetwt").innerHTML = parts[2];
                    document.getElementById("viewbtgrosswt").innerHTML = parts[3];
                    document.getElementById("bremain").innerHTML = parts[1]-parts[4];
                    document.getElementById("bused").innerHTML = parts[4];
                    document.getElementById("bremainnetwt").innerHTML = parts[2]-parts[5];
                    document.getElementById("bremaingrosswt").innerHTML = parts[3]-parts[6];
                    document.getElementById("busednetwt").innerHTML = parts[5];
                    document.getElementById("busedgrosswt").innerHTML = parts[6];
                }
            }
            
        }
    };
    xhttp.open("GET", "phpback/search_dates.php?db="+db, true);
    xhttp.send();
}

</script>



