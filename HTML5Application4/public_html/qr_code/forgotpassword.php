<?php 
    
     include("connection.php");

?>

<html>
    <head>
        <title>Forgot Password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="shortcut icon" href="images/applogo.jpg">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link  rel="stylesheet" href = "css/logincss.css">
    </head>
    <body>
        <div class="container-fluid d-flex justify-content-center align-items-center" style ='height: 100%;'> 
           <div class="row">
               <div class = 'container p-5 '>
                      <img src="images/avatar.png" class="avatar">
                      <h1 style = 'margin-bottom: 20px;'>Forgot Password</h1>
                    <form  action = '' method = 'POST'>
                        <p>Mail ID</p>
                        <input type="text" name="mail" placeholder="Mail ID" required="" autocomplete="off">
                        <input style = "margin-top: 20px;" type="submit" value= "Submit">
                        <input style = "margin-top: 0px;" type="submit" onclick="window.history.back()" value= "Back">
                    </form>
                </div>       
            </div>
        </div>
    </body>
</html>

<script>
  const togglePassword = document.querySelector('#togglePassword');
  const password = document.querySelector('#password');

  togglePassword.addEventListener('click', function (e) {
  const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
  password.setAttribute('type', type);
  this.classList.toggle('fa-eye-slash');
});
</script>