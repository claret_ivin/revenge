
  $(document).ready(function(){
    $("#myInput").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#myTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });

// check network before create qrcode
function networkcheck()
{
    if(navigator.onLine == true)
    {
       return true;
    }
    else
    {
       return false;
    }
}

// add new purchase customer 
function purchase_customer(val)
{
  if(document.getElementById(val).value == 'New')
  {
      let name = prompt('Create new purchase customer');
      if(name != null && name != '')
      {
          let option = document.createElement("option");
          option.text = name;
         
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() 
          {
            if(this.readyState == 4 && this.status == 200)
            {
               if(this.responseText == 'exist')
               {
                  alert("Name already in database");
                  document.getElementById(val).value = option.text;
               }
               else if(this.responseText == 'true')
               {
                 document.getElementById(val).add(option);
                 document.getElementById(val).value = option.text;
               }
               else
               {
                 alert("Not created new customer.")
                 document.getElementById(val).value = 'Suppliers';
               }
            }
            
          };
          xhttp.open("POST", "phpback/customer_post.php", true);
          xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhttp.send("name="+option.text+"&db=purchase_customer"); 
      }
      else{
         document.getElementById(val).value = 'Suppliers';
      }
  }
}

// list creation.
var listbool = false;
function createlist()
{
   if(listbool == false)
   {
      let msg = '';
      if(document.getElementById('customer').value == 'Purchase Customer')
      {
          msg = '◉ Select the Purchase Customer';
      }  
      if(document.getElementById('date').value == '')
      {
          msg += '\n◉ Select the Date';
      }
      if(document.getElementById('pno').value == '')
      {
          msg += '\n◉ Enter the Purchase Number';
      }
      if(document.getElementById('lsize').value == '')
      {
          msg = '◉ Select the size';
      } 
      if(msg != '')
      {
         alert(msg);
      }
      else
      {
          listbool = true;
          $('.idbtn').removeClass('btn-primary').addClass('btn-danger');
          document.querySelector('.idbtn').innerHTML = 'Close List';
          let d = new Date();
          let id = d.getFullYear()+''+d.getMonth()+''+d.getDate()+''+d.getHours()+''+d.getMinutes()+''+d.getSeconds();
          document.getElementById('unid').value = id;
          store_list();
      }
   }
   else
   {
      listbool = false;
      $('.idbtn').removeClass('btn-danger').addClass('btn-primary');
      document.querySelector('.idbtn').innerHTML = 'Create New List';
      document.getElementById('unid').value = '';
      document.getElementById('pno').value = '';
      document.getElementById('date').value ='';
      document.getElementById('lsize').value ='210D';
      document.getElementById('customer').value ='Suppliers';
      var canvas = document.getElementById('canvas'),
      ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, canvas.width, canvas.height);
   }
}

// create list will store in database
function store_list()
{
    let customer = document.getElementById('customer').value;
    let uid = document.getElementById('unid').value;
    let pronum = document.getElementById('pno').value;
    let date = document.getElementById('date').value;
    let lsize = document.getElementById('lsize').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
           if(this.responseText == 'success')
           {
              document.getElementById('submitmsg').innerHTML = "The purchase list was successfully created.";
              document.getElementById('submitstatus').style.display = 'block';
               $('#submitstatus').delay(5000).hide(0); 

           }
           else
           {
              alert('Purchase list was not created.');
              listbool = false;
              $('.idbtn').removeClass('btn-danger').addClass('btn-primary');
              document.querySelector('.idbtn').innerHTML = 'Create New List';
              document.getElementById('unid').value = "";
           }
        }
    };
    xhttp.open("POST", "phpback/create_list.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("customer="+customer+"&uniqueid="+uid+"&number="+pronum+"&date="+date+"&size="+lsize+"&db=production"); 
}


// items verification 
function items_verification(boxno,size,netwt,grosswt)
{
      let msg = '';
      if(boxno == '')
      {
          msg = '◉ Enter Box Number';
      }  
      if(size == '')
      {
          msg += '\n◉ Select the Size';
      }
      if(netwt == '')
      {
          msg += '\n◉ Enter the Net Weight';
      }
      if(grosswt == '')
      {
          msg += '\n◉ Enter the Gross Weight';
      }
      if(msg != '')
      {
         alert(msg);
         return false;
      }
      else
      {
        return true;
      }
}

// store bags in list
function store_bags()
{
  if(document.getElementById('unid').value != '')
  {
    let uid = document.getElementById('unid').value;
    let boxno = document.getElementById('boxnumber').value;
    let size = document.getElementById('size').value;
    let date = document.getElementById('date').value;
    let netwt = document.getElementById('netwt').value;
    let grosswt = document.getElementById('grosswt').value;
    let canvas = document.getElementById('canvas');
    if(items_verification(boxno,size,netwt,grosswt))
    {
       if(networkcheck())
       {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() 
          {
              if(this.readyState == 4 && this.status == 200)
              {
                 var parts = this.responseText.split('$');
                 if(parts[0] == 'success')
                 {
                    document.getElementById('submitmsg').innerHTML="The purchase bag was successfully added.";
                    document.getElementById('submitstatus').style.display = 'block';
                    document.getElementById('boxnumber').value = '';
                    document.getElementById('size').value = '210D';
                    document.getElementById('netwt').value = '';
                    document.getElementById('grosswt').value = '';
                    generate_code(uid,boxno,size,0);
                    $('#submitstatus').delay(5000).hide(0); 
                 }
                 else if(parts[0] == 'exist')
                 {
                    alert("Box Number already exist in this list.");
                 }
                 else
                 {
                    alert("QRCODE was not generated.");
                 }
              }
            };
            xhttp.open("POST", "phpback/store_bags.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("key=0&uniqueid="+uid+"&boxnumber="+boxno+"&size="+size+"&date="+date+"&netwt="+netwt+"&grosswt="+grosswt+"&canvas="+canvas+"&db=production_boxes&bdb=production");
       }
       else
       {
            alert("No internet available to generate qrcode.");
       }
    }
  }
  else
  {
    alert("First create the purchase list then add items(boxes).");
  }
}

// generate qr code
function generate_code(uid,boxno,size,key)
{
    var image = new Image();
    image.src = "phpback/qrimages/"+uid+"-"+boxno+".png";
    var canvas = document.getElementById('canvas'),
    ctx = canvas.getContext('2d');
    canvas.width = 250;
    canvas.crossOrigin = "Anonymous";
    canvas.height = 250;
    ctx.font = "10pt Verdana";
    image.onload = function()
    {
        ctx.drawImage(image, 0, 0);
        ctx.fillStyle = "red";
        ctx.fillText(boxno,35,canvas.height-30);
        ctx.fillStyle = "green";
        ctx.fillText('3ply',canvas.width-85,canvas.height-30);
        document.getElementById('qrcode').src = canvas.toDataURL();
        qrcode_submit(uid,boxno,canvas.toDataURL("image/png"),key);
    }
    if(key == 1){canvas.style.display='none';}else{canvas.style.display='block';} 
} 

// generate qrcode submit 
function qrcode_submit(uid,boxno,qrcode,key)
{
     var xhttp = new XMLHttpRequest();
     xhttp.onreadystatechange = function() 
     {
        if(this.readyState == 4 && this.status == 200)
        {
             var parts = this.responseText.split('$');
             if(parts[0] == 'submit')
             {  
                if(key == 1)
                {
                    document.getElementById("viewtable").innerHTML = parts[1];
                    document.getElementById("viewtbags").innerHTML = parts[2];
                    document.getElementById("viewtnetwt").innerHTML = parts[3];
                    document.getElementById("viewtgrosswt").innerHTML = parts[4];
                    document.getElementById("remain").innerHTML = parts[2]-parts[5];
                    document.getElementById("used").innerHTML = parts[5];
                    document.getElementById("remainnetwt").innerHTML = parts[3]-parts[6];
                    document.getElementById("remaingrosswt").innerHTML = parts[4]-parts[7];
                    document.getElementById("usednetwt").innerHTML = parts[6];
                    document.getElementById("usedgrosswt").innerHTML = parts[7];
                }
             }
             else{
                //
             }
        }
     };
 
     xhttp.open("POST", "phpback/update_qrcode.php", true);
     xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
     xhttp.send("key="+key+"&uniqueid="+uid+"&boxnumber="+boxno+"&qrcode="+qrcode+"&db=production_boxes&bdb=production");
}


// search size in the list
function searchsize()
{
  var str = document.getElementById("ssize").value;
  var xhttp;
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() 
      {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('$');
            if(parts[1] == 'false')
            {
                alert(str+" size not in database...");
                document.getElementById("ssize").value = "All Size";
                document.getElementById("myTable").innerHTML = parts[0];
            }
            else{
                document.getElementById("myTable").innerHTML = parts[0];
            }
        }
      };
      xhttp.open("GET", "phpback/searchsize.php?db=production&size="+str, true);
      xhttp.send();  
}


// search list with dates
function datesearch()
{
    let fdate = document.getElementById('fromdate').value;
    let tdate = document.getElementById('todate').value;
    if(fdate != '')
    {
        if(tdate == '')
        {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() 
            {
              if(this.readyState == 4 && this.status == 200)
              {
                  var parts = this.responseText.split('$');
                  if(parts[1] == 'false')
                  {
                      //
                  }
                  else{
                      document.getElementById("myTable").innerHTML = parts[0];
                  }
              }
            };
            xhttp.open("GET", "phpback/search_dates.php?db=production&fdate="+fdate, true);
            xhttp.send();
        }
        else
        {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() 
            {
              if(this.readyState == 4 && this.status == 200)
              {
                  var parts = this.responseText.split('$');
                  if(parts[1] == 'false')
                  {
                      //
                  }
                  else{
                      document.getElementById("myTable").innerHTML = parts[0];
                  }
              }
            };
            xhttp.open("GET", "phpback/search_dates.php?db=production&fdate="+fdate+"&tdate="+tdate, true);
            xhttp.send();
        }
    }
    else{
      alert("You want to select from date.");
    }
}

// to clear the dates.
function clear_dates()
{

    document.getElementById('fromdate').value = '';
    document.getElementById('todate').value = '';
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var parts = this.responseText.split('|');
            if(parts[1] == 'false')
            {
                document.getElementById("myTable").innerHTML = parts[0];
            }
            else{
                document.getElementById("myTable").innerHTML = parts[0];
            }
        }
    };
    xhttp.open("GET", "phpback/search_dates.php?db=production", true);
    xhttp.send();
}

// change the text type to date
function datechange(val)
{
     val.type = 'date';
     //
}

// change the date type to text
function textchange(val)
{
  val.type = 'text';
  //pass
}


// table view , edit , delete in search.
function tablevalues(val)
{
    let id = val.childNodes[1].innerHTML;  
    let pcustomer = val.childNodes[0].innerHTML;
    let size = val.childNodes[2].innerHTML;
    let pno = val.childNodes[3].innerHTML;
    let pdate = val.childNodes[4].innerHTML;
    let bags = val.childNodes[5].innerHTML; 
    let totalnetwt = val.childNodes[6].innerHTML;
    let totalgrosswt = val.childNodes[7].innerHTML; 
    
    if(editbool == true || deletebool == true)
    {
        if(editbool == true)
        {
            document.getElementById('ecustomer').value = val.childNodes[0].innerHTML;
            document.getElementById('euid').value = val.childNodes[1].innerHTML;
            document.getElementById('elsize').value = val.childNodes[2].innerHTML;
            document.getElementById('eprono').value = val.childNodes[3].innerHTML;
            document.getElementById('epdate').value = val.childNodes[4].innerHTML;
            $('#myModal').modal('show');
        }
        else
        {    document.getElementById('duniqueid').value = val.childNodes[1].innerHTML;
             $('#myModal2').modal('show');  
        }
    }
    /*else
    {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() 
        {
          if(this.readyState == 4 && this.status == 200)
          {
              var parts = this.responseText.split('$');
              document.getElementById("viewuid").innerHTML = id;
              document.getElementById("viewpno").innerHTML = pno;
              document.getElementById("viewpcustomer").innerHTML = pcustomer;
              document.getElementById("viewsize").innerHTML = size;
              document.getElementById("viewdate").innerHTML = pdate;
              document.getElementById("viewtbags").innerHTML = bags;
              document.getElementById("viewtnetwt").innerHTML = totalnetwt;
              document.getElementById("viewtgrosswt").innerHTML = totalgrosswt;

              document.getElementById("viewtable").innerHTML = parts[0];
              document.getElementById("remain").innerHTML = bags-parts[1];
              document.getElementById("used").innerHTML = parts[1];
              document.getElementById("remainnetwt").innerHTML = totalnetwt-parts[2];
              document.getElementById("remaingrosswt").innerHTML = totalgrosswt-parts[3];
              document.getElementById("usednetwt").innerHTML = parts[2];
              document.getElementById("usedgrosswt").innerHTML = parts[3];
          }
        };
        xhttp.open("GET", "phpback/searchview.php?id="+id+"&type=production_boxes", true);
        xhttp.send(); 
        document.getElementById("viewid").click();
    }  */
}

function table_view_doubleclick(val)
{
    let id = val.childNodes[1].innerHTML;  
    let pcustomer = val.childNodes[0].innerHTML;
    let size = val.childNodes[2].innerHTML;
    let pno = val.childNodes[3].innerHTML;
    let pdate = val.childNodes[4].innerHTML;
    let bags = val.childNodes[5].innerHTML; 
    let totalnetwt = val.childNodes[6].innerHTML;
    let totalgrosswt = val.childNodes[7].innerHTML; 
    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() 
        {
          if(this.readyState == 4 && this.status == 200)
          {
              var parts = this.responseText.split('$');
              document.getElementById("viewuid").innerHTML = id;
              document.getElementById("viewpno").innerHTML = pno;
              document.getElementById("viewpcustomer").innerHTML = pcustomer;
              document.getElementById("viewsize").innerHTML = size;
              document.getElementById("viewdate").innerHTML = pdate;
              document.getElementById("viewtbags").innerHTML = bags;
              document.getElementById("viewtnetwt").innerHTML = totalnetwt;
              document.getElementById("viewtgrosswt").innerHTML = totalgrosswt;

              document.getElementById("viewtable").innerHTML = parts[0];
              document.getElementById("remain").innerHTML = bags-parts[1];
              document.getElementById("used").innerHTML = parts[1];
              document.getElementById("remainnetwt").innerHTML = totalnetwt-parts[2];
              document.getElementById("remaingrosswt").innerHTML = totalgrosswt-parts[3];
              document.getElementById("usednetwt").innerHTML = parts[2];
              document.getElementById("usedgrosswt").innerHTML = parts[3];
          }
        };
        xhttp.open("GET", "phpback/searchview.php?id="+id+"&type=production_boxes", true);
        xhttp.send(); 
        document.getElementById("viewid").click();
}

var deletebool = false;

function deletetable()
{
   if(deletebool == true)
   {
      deletebool = false;
      $('.btn2').removeClass('btn-danger').addClass('btn-primary ');
   }
   else{
      deletebool = true;
      $('.btn2').removeClass('btn-primary').addClass('btn-danger');
   }
}

var editbool = false;

function edittable()
{
   if(editbool == true)
   {
      editbool = false;
      $('.btn1').removeClass('btn-danger').addClass('btn-primary ');
   }
   else{
      editbool = true;
      $('.btn1').removeClass('btn-primary').addClass('btn-danger');
   }
}

// table edit submit in search.
function editsubmit()
{
    var uid =  document.getElementById('euid').value;
    var customer = document.getElementById('ecustomer').value;
    var pronum =  document.getElementById('eprono').value;
    var date = document.getElementById('epdate').value;
    var lsize = document.getElementById('elsize').value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200)
     {
        var parts = this.responseText.split('$');
        if(parts[1] != 'false')
        {
            document.getElementById("ssize").value = "All Size";
            document.getElementById("myTable").innerHTML = parts[0];
        }
        else
        {
           $('#myModal').modal('hide');  
           alert('Not updated.');
        }
      }
    };
    xhttp.open("POST", "phpback/update.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   xhttp.send("uid="+uid+"&customer="+customer+"&size="+lsize+"&num="+pronum+"&date="+date+"&db=production");
}

// table delete submit in search.
function deletesubmit()
{
    var uniqueid = document.getElementById('duniqueid').value;
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200)
      {
          var parts = this.responseText.split('$');
          if(parts[1] == 'false')
          {
              document.getElementById("ssize").value = "All Size";
              document.getElementById("myTable").innerHTML = '';
          }
          else{
              document.getElementById("ssize").value = "All Size";
              document.getElementById("myTable").innerHTML = parts[0];
            }
      }
    };
    xhttp.open("POST", "phpback/delete.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("uniqueid="+uniqueid+"&db=production&sdb=production_boxes");
}

// add new item verificetion
function add_new_item_verification()
{
      let msg = '';
      if(document.getElementById('aboxnumber').value == '')
      {
          msg = '◉ Enter Box Number';
      }  
      if(document.getElementById('asize').value == '')
      {
          msg += '\n◉ Select the Size';
      }
      if(document.getElementById('adate').value == '')
      {
          msg += '\n◉ Select the Date';
      }
      if(document.getElementById('anetwt').value == '')
      {
          msg += '\n◉ Enter the Net Weight';
      }
      if(document.getElementById('agrosswt').value == '')
      {
          msg += '\n◉ Enter the Gross Weight';
      }
      if(msg != '')
      {
         alert(msg);
      }
      else
      {
         add_bag_submit();
      }
}

// add new item in view.
function add_new_bag()
{
    if(document.getElementById('viewuid').innerHTML != '')
    {
      $('#myModal3').modal('show');
    }
    else
    {
      alert("Select the list First then add new Item(Bag).");
    }
}

// add new item in view submit.
function add_bag_submit()
{ 
    let uid = document.getElementById('viewuid').innerHTML;
    let boxno = document.getElementById('aboxnumber').value;
    let size = document.getElementById('asize').value;
    let date = document.getElementById('adate').value;
    let netwt = document.getElementById('anetwt').value;
    let grosswt = document.getElementById('agrosswt').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
           var parts = this.responseText.split('$');
           if(parts[0] == 'success')
           {
              generate_code(uid,boxno,size,1);
           }
           else if(parts[0] == 'exist')
           {
              $('#myModal3').modal('hide');
              alert("Box Number already exist in this list.");
           }
           else{
              $('#myModal3').modal('hide');
              alert("QRCODE not generated.");
           }
        }
    };
    xhttp.open("POST", "phpback/store_bags.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("key=1&uniqueid="+uid+"&boxnumber="+boxno+"&size="+size+"&date="+date+"&netwt="+netwt+"&grosswt="+grosswt+"&bdb=production&db=production_boxes");
  
}

var editviewbool = false;
function edittableviewbool()
{
    if(document.getElementById('viewuid').innerHTML != '')
    {
        if(editviewbool == true)
        {
           editviewbool = false;
           $('.viewbtn1').removeClass('btn-danger').addClass('btn-primary ');
        }
        else
        {
           editviewbool = true;
           $('.viewbtn1').removeClass('btn-primary').addClass('btn-danger');
        }
    }
    else
    {
      alert("Select the list First then edit Item(Bag).");
    }
}

var deleteviewbool = false;
function deletetableviewbool()
{
    if(document.getElementById('viewuid').innerHTML != '')
    {
        if(deleteviewbool == true)
        {
           deleteviewbool = false;
           $('.viewbtn2').removeClass('btn-danger').addClass('btn-primary ');
        }
        else
        {
           deleteviewbool = true;
           $('.viewbtn2').removeClass('btn-primary').addClass('btn-danger');
        }
    }
    else
    {
      alert("Select the list First then delete Item(Bag).");
    }
}


// view edit table view .
function edittableview(val)
{
    if(editviewbool == true)
    {
        document.getElementById('veuid').value = document.getElementById('viewuid').innerHTML;
        document.getElementById('vecustomer').value = document.getElementById('viewpcustomer').innerHTML;
        document.getElementById('velsize').value = document.getElementById('viewsize').innerHTML;
        document.getElementById('veprono').value = document.getElementById('viewpno').innerHTML;
        document.getElementById('vepdate').value = document.getElementById('viewdate').innerHTML;
        document.getElementById('veboxnumber').value = val.childNodes[0].innerHTML;
        document.getElementById('vesize').value = val.childNodes[1].innerHTML;
        document.getElementById('vedate').value = val.childNodes[2].innerHTML;
        document.getElementById('venetwt').value = val.childNodes[3].innerHTML;
        document.getElementById('vegrosswt').value = val.childNodes[4].innerHTML;
        document.getElementById('vescanneddate').value = val.childNodes[5].innerHTML;
        $('#myModal4').modal('show');
    }
    else if(deleteviewbool == true)
    {
        document.getElementById('vduniqueid').value = document.getElementById('viewuid').innerHTML;
        document.getElementById('vdboxnumber').value = val.childNodes[0].innerHTML;
        netweight_old = val.childNodes[3].innerHTML;
        grossweight_old = val.childNodes[4].innerHTML;
        $('#myModal5').modal('show');
    }
    else
    {
        document.getElementById('codeid').value = document.getElementById('viewuid').innerHTML;
        document.getElementById('codeno').value = val.childNodes[0].innerHTML;
    }
}

// to submit the the view edits.
function vieweditsubmit()
{
    let uid = document.getElementById('viewuid').innerHTML;
    let esupplier = document.getElementById('viewpcustomer').value;
    let vesize = document.getElementById('vesize').value;
    let vepno = document.getElementById('veprono').value;
    let vepdate = document.getElementById('vepdate').value;
    let boxno = document.getElementById('veboxnumber').value;
    let size = document.getElementById('vesize').value;
    let date = document.getElementById('vedate').value;
    let netwt = document.getElementById('venetwt').value;
    let grosswt = document.getElementById('vegrosswt').value;
    let scanneddate = document.getElementById('vescanneddate').value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if(this.readyState == 4 && this.status == 200)
        {
           var parts = this.responseText.split('$');
           if(parts[1] == 'notsubmit')
           {
              $('#myModal4').modal('hide');
              alert("Not updated.");
           }
           else if(parts[1] == 'false')
           {
              alert("table not loaded");
           }
           else
           {
                document.getElementById("viewtable").innerHTML = parts[0];
                document.getElementById("viewtnetwt").innerHTML = parts[2];
                document.getElementById("viewtgrosswt").innerHTML = parts[3];
                
                document.getElementById("remain").innerHTML = parts[1]-parts[4];
                document.getElementById("used").innerHTML = parts[4];
                document.getElementById("remainnetwt").innerHTML = parts[2]-parts[5];
                document.getElementById("remaingrosswt").innerHTML = parts[3]-parts[6];
                document.getElementById("usednetwt").innerHTML = parts[5];
                document.getElementById("usedgrosswt").innerHTML = parts[6];
           }
        }
    };
    xhttp.open("POST", "phpback/update_bags.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("uid="+uid+"&boxnumber="+boxno+"&size="+size+"&date="+date+"&netwt="+netwt+"&grosswt="+grosswt+"&sdate="+scanneddate+"&pcustomer="+esupplier+"&lsize="+vesize+"&lpno="+vepno+"&lpdate="+vepdate+"&db=production_boxes&bdb=production"); 
}

var netweight_old,grossweight_old; // to update weights while deleting
function deletetableviewsubmit()
{
    let uniqueid = document.getElementById('vduniqueid').value;
    let bagnumber = document.getElementById('vdboxnumber').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            var parts = this.responseText.split('$');
            if(parts[1] == 'notdelete')
            {
                alert('Item(Bag) not deleted.');
            }
            else if(parts[1] == 'false')
            {
                 document.getElementById("viewtable").innerHTML = '';
                 document.getElementById("viewtbags").innerHTML = 0;
                 document.getElementById("viewtnetwt").innerHTML = 0;
                 document.getElementById("viewtgrosswt").innerHTML = 0;
            }
            else{
                document.getElementById("viewtable").innerHTML = parts[0];
                document.getElementById("viewtbags").innerHTML = parts[1];
                document.getElementById("viewtnetwt").innerHTML = parts[2];
                document.getElementById("viewtgrosswt").innerHTML = parts[3];
                document.getElementById("remain").innerHTML = parts[1]-parts[4];
                document.getElementById("used").innerHTML = parts[4];
                document.getElementById("remainnetwt").innerHTML = parts[2]-parts[5];
                document.getElementById("remaingrosswt").innerHTML = parts[3]-parts[6];
                document.getElementById("usednetwt").innerHTML = parts[5];
                document.getElementById("usedgrosswt").innerHTML = parts[6];
            }
        }
    };
    xhttp.open("POST", "phpback/delete_bags.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("uniqueid="+uniqueid+"&bagnumber="+bagnumber+"&oldnetwt="+netweight_old+"&oldgrosswt="+grossweight_old+"&db=production_boxes&bdb=production");
}

// remove scanned date from the list
function remove_sdate()
{
  document.getElementById('vescanneddate').value = "";
}

// to show the particular qrcode
function showqrcode(val)
{
    document.getElementById('modalimg').src = val.src;
    $('#myModal6').modal('show'); 
}

              
function imagecheck()
{
   const blank = document.createElement('canvas');

    blank.width = canvas.width;
    blank.height = canvas.height;

    if(canvas.toDataURL() === blank.toDataURL())
    {
      alert("No Qrcode.");
    }
    else
    {
        var contents = document.getElementById("img").innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame";
        frame1.style.position = "absolute";
        frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html><head><title>Qrcode</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
        window.frames["frame"].focus();
        window.frames["frame"].print();
        document.body.removeChild(frame1);
        }, 500);
      
    }
} 

function imagecheck2()
{
        var contents = document.getElementById("modalimgdiv").innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame";
        frame1.style.position = "absolute";
        frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html><head><title>Qrcode</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
        window.frames["frame"].focus();
        window.frames["frame"].print();
        document.body.removeChild(frame1);
        }, 500);
    
} 



