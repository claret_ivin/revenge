<?php 
    
     include("connection.php");

     if(isset($_COOKIE['id']))
     {
       header("Location: Home");
     }

if(!empty($_POST['password']))
{
    $sql = "SELECT * FROM login";

    $result = mysqli_query($conn,$sql);

    $check = mysqli_num_rows($result);
    if($check > 0)
    {
        $name = $_POST['name'];
        $pass = $_POST['password'];
        
        while($row = mysqli_fetch_assoc($result))
        {
            if($name == $row['username'] & $pass == $row['password'])
            {
                setcookie("id",'11101',time() + (86400 * 7));
               
                header("Location: Home?login=succeed");
            }
        }

        $error1 = '#worng username';
        $error2 = '#worng password';
   }
    else
    {
    echo 'something went wrong reload it!';
    }
}
?>

<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="shortcut icon" href="images/applogo.jpg">
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link  rel="stylesheet" href = "css/logincss.css">
    </head>
    <body>
        <div class="container-fluid d-flex justify-content-center align-items-center" style ='height: 100%;'> 
           <div class="row">
               <div class = 'container p-5 '>
                      <img src="images/avatar.png" class="avatar">
                      <h1>Login Here</h1>
                    <form  action = '' method = 'POST'>
                        <p>UserName</p>
                        <input type="text" name="name" placeholder="username" required="" autocomplete="off">
                        <p style = 'font-size: 13px;color : red;margin-bottom : 10px;'><?php if(isset($error1)) echo $error1; ?></p>
                        <p>Password</P>
                        <input type="password" id = 'password' name="password" placeholder="password" required="">  
                         <i class="far fa-eye eyes" id="togglePassword"></i>
                         <p style = 'font-size: 13px;color : red;margin-bottom : 10px;'><?php if(isset($error2)) echo $error2; ?></p> 
                        <input type="submit" name="" value="Login">
                        <a href="ForgotPassword">Forgot Password</a>
                        <a style = "margin-left: 20px;" href="SignUp">SignUp</a><br>
                    </form>
                </div>       
            </div>
        </div>
    </body>
</html>

<script>
  const togglePassword = document.querySelector('#togglePassword');
  const password = document.querySelector('#password');

  togglePassword.addEventListener('click', function (e) {
  const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
  password.setAttribute('type', type);
  this.classList.toggle('fa-eye-slash');
});
</script>